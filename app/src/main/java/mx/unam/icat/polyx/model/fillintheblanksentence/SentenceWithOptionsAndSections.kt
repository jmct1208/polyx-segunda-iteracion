package mx.unam.icat.polyx.model.fillintheblanksentence

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import mx.unam.icat.polyx.model.fillintheblankincorrectoption.FillInTheBlankIncorrectOption
import mx.unam.icat.polyx.model.fillintheblankincorrectoption.FillInTheBlankSentenceFillInTheBlankIncorrectOption
import mx.unam.icat.polyx.model.fillintheblankcorrectoption.FillInTheBlankCorrectOption

data class SentenceWithOptionsAndSections(
    @Embedded var fillInTheBlankSentence: FillInTheBlankSentence,
    @Relation(
        parentColumn = "fill_in_the_blank_sentence_id",
        entityColumn = "fill_in_the_blank_sentence_owner_id",
        entity = FillInTheBlankCorrectOption::class
    )
    val fillInTheBlankCorrectOptions: List<FillInTheBlankCorrectOption>,

    @Relation(
        parentColumn = "fill_in_the_blank_sentence_id",
        entityColumn = "fill_in_the_blank_incorrect_option_id",
        entity = FillInTheBlankIncorrectOption::class,
        associateBy = Junction(
            value = FillInTheBlankSentenceFillInTheBlankIncorrectOption::class,
            parentColumn = "fill_in_the_blank_sentence_owner_id",
            entityColumn = "fill_in_the_blank_incorrect_option_owner_id"
        )
    )
    val fillInTheBlankIncorrectOptions: List<FillInTheBlankIncorrectOption>
)