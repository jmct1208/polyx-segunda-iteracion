package mx.unam.icat.polyx.model.fillintheblanksentence

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SentenceRepository @Inject constructor(private val fillInTheBlankSentenceDao: FillInTheBlankSentenceDao){
    fun getSentenceWithOptionsAndSectionsByUnitId(unitId: Long) :
            Flow<List<SentenceWithOptionsAndSections>> {
        return fillInTheBlankSentenceDao.getSentencesWithOptionsAndSectionsByUnitId(unitId)
    }
}