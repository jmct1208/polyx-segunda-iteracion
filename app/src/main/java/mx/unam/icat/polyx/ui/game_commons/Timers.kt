package mx.unam.icat.polyx.ui.game_commons

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ProgressIndicatorDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import mx.unam.icat.polyx.ui.theme.PolyxTheme

@Composable
fun ProgressIndicator(
    progress: Float,
    modifier: Modifier = Modifier
) {

    val animatedProgress by animateFloatAsState(
        targetValue = progress,
        animationSpec = ProgressIndicatorDefaults.ProgressAnimationSpec, label = "Timer animation"
    )
    CircularProgressIndicator(
        progress = animatedProgress,
        strokeWidth = 10.dp,
        modifier = modifier
    )
}

@Composable
fun CountDownTimer(
    timeRemaining: Int,
    timeAvailable: Int,
    modifier: Modifier = Modifier
) {
    val progress = timeRemaining / timeAvailable.toFloat()
    Box(modifier = modifier.size(100.dp)) {
        ProgressIndicator(
            progress,
            modifier = Modifier
                .align(Alignment.Center)
                .scale(scaleX = -1f, scaleY = 1f)
                .fillMaxSize()
        )
        Text(text = timeRemaining.toString(), modifier = Modifier.align(Alignment.Center))
    }
}

@Preview(showBackground = true)
@Composable
fun TimerPreview() {
    PolyxTheme {
        CountDownTimer(timeRemaining = 14, timeAvailable = 15)
    }
}
