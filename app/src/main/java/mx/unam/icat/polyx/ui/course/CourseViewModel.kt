package mx.unam.icat.polyx.ui.course

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.toRoute
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import mx.unam.icat.polyx.CourseRoute
import mx.unam.icat.polyx.model.gameTypes.GameType
import mx.unam.icat.polyx.model.gameTypes.GameTypeRepository
import mx.unam.icat.polyx.model.section.SectionEntity
import mx.unam.icat.polyx.model.section.SectionRepository
import javax.inject.Inject

@HiltViewModel
class CourseViewModel @Inject constructor(
    sectionRepository: SectionRepository,
    gameTypeRepository: GameTypeRepository,
    state: SavedStateHandle,
) : ViewModel() {
    private val _units = MutableStateFlow<UnitListUiState>(UnitListUiState.Loading)
    val units = _units.asStateFlow()
    private val _gameTypes = MutableStateFlow<GameTypesUiState>(GameTypesUiState.Loading)
    val gameTypes = _gameTypes.asStateFlow()


    init {
        viewModelScope.launch {
            val courseName = state.toRoute<CourseRoute>().courseName
            val courseId = sectionRepository.getCourseIdByTitle(courseName)
            _units.value = if (courseId != null) {
                UnitListUiState.Success(
                    units = sectionRepository.getUnitsOfCourse(courseId),
                )
            } else {
                UnitListUiState.Error
            }
            _gameTypes.value = if (courseId != null) {
                GameTypesUiState.Success(
                    courseId = courseId,
                    gameTypeRepository.getGameTypes())
            } else {
                GameTypesUiState.Error
            }
        }
    }
}

sealed interface UnitListUiState {
    data object Loading : UnitListUiState
    data class Success(val units: List<SectionEntity>) : UnitListUiState
    data object Error : UnitListUiState
}

sealed interface GameTypesUiState {
    data object Loading : GameTypesUiState
    data class Success(val courseId: Long, val gameTypes: List<GameType>) : GameTypesUiState
    data object Error : GameTypesUiState
}

