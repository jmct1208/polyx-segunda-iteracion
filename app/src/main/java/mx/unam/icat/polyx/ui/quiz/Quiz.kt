package mx.unam.icat.polyx.ui.quiz

import android.content.res.Configuration
import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.core.TweenSpec
import androidx.compose.animation.core.tween
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import mx.unam.icat.polyx.ui.game_commons.CountDownTimer
import mx.unam.icat.polyx.ui.game_commons.GameScreen
import mx.unam.icat.polyx.ui.game_commons.QuestionWrapper
import mx.unam.icat.polyx.ui.game_commons.QuitGameDialog
import mx.unam.icat.polyx.ui.game_commons.options1
import mx.unam.icat.polyx.ui.game_commons.question1
import mx.unam.icat.polyx.ui.theme.PolyxTheme
import mx.unam.icat.polyx.ui.theme.md_theme_dark_outline
import mx.unam.icat.polyx.ui.theme.md_theme_light_outline
import mx.unam.icat.polyx.utils.PausableCountDownTimer

private const val CONTENT_ANIMATION_DURATION = 300

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun TextOptionSelectedPreview() {
    PolyxTheme {
        Column {
            TextOption("¿Quién es ternura 68?", true, {})
            TextOption("¿Quién es ternura 68?", false, {})
        }

    }
}

@Composable
fun TextOption(
    text: String,
    selected: Boolean,
    onOptionSelected: () -> Unit,
    modifier: Modifier = Modifier
) {
    Surface(
        shape = MaterialTheme.shapes.small,
        color = if (selected) {
            MaterialTheme.colors.background
        } else {
            MaterialTheme.colors.surface
        },
        border = BorderStroke(
            width = 1.dp,
            color = if (selected) {
                MaterialTheme.colors.primary
            } else {
                if (isSystemInDarkTheme()) {
                    md_theme_dark_outline
                } else {
                    md_theme_light_outline
                }
            }
        ),
        modifier = modifier
            .clip(MaterialTheme.shapes.small)
            .selectable(
                selected = selected,
                onClick = onOptionSelected,
                role = Role.RadioButton
            )
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Text(text, style = MaterialTheme.typography.body2)
        }
    }
}


@Composable
fun Options(
    options: List<String>,
    selectedOption: Int?,
    onOptionSelected: (Int) -> Unit
) {
    options.forEachIndexed { index, option ->
        val selected = index == selectedOption
        TextOption(
            text = option,
            selected = selected,
            onOptionSelected = { onOptionSelected(index) }
        )
        Spacer(modifier = Modifier.height(16.dp))
    }
}

@Composable
fun QuizQuestion(
    questionText: String,
    options: List<String>,
    selectedOption: Int?,
    onOptionSelected: (Int) -> Unit,
    modifier: Modifier = Modifier
) {
    QuestionWrapper(
        questionText = questionText,
        modifier = modifier.selectableGroup()
    ) {
        Options(
            options = options,
            selectedOption = selectedOption,
            onOptionSelected = onOptionSelected
        )
    }
}

@Preview(showBackground = true)
@Composable
fun QuizQuestionAndGameBars() {
    PolyxTheme {
        GameScreen(
            onCheckPressed = {},
            isCheckEnabled = true,
            onNextPressed = {},
            isNextEnabled = true,
            navigateUp = {},
            onChangeThemePressed = {},
            timerContent = { modifier: Modifier ->
                CountDownTimer(
                    timeAvailable = 14,
                    timeRemaining = 15,
                    modifier = modifier
                )
            },
            answer = null,
            questionIndex = 1,
            questionCount = 14
        ) {
            val longerOptions = options1
            QuizQuestion(
                questionText = question1.text,
                options = longerOptions.map { it.text!! },
                selectedOption = null,
                onOptionSelected = {},
            )
        }
    }
}

@Composable
fun QuizScreen(
    viewModel: QuizViewModel = hiltViewModel(),
    navigateUp: () -> Unit,
    navigateToResults: () -> Unit,
) {
    val quizUiState by viewModel.quizUiState.collectAsState()
    when (val state = quizUiState) {
        QuizUiState.Loading -> {}
        QuizUiState.QuizOver -> navigateToResults()
        is QuizUiState.Success -> SuccessQuizScreen(
            state = state,
            navigateUp = navigateUp,
            startTimer = viewModel::startTimer,
            pauseTimer = viewModel::pauseTimer,
            onOptionSelected = viewModel::optionClicked,
            answerQuestion = viewModel::answerQuestion,
            onNextPressed = viewModel::changeToNextQuestion
        )
    }
}

@Composable
fun SuccessQuizScreen(
    state: QuizUiState.Success,
    navigateUp: () -> Unit,
    startTimer: () -> Unit,
    pauseTimer: () -> Unit,
    onOptionSelected: (Int) -> Unit,
    answerQuestion: () -> Unit,
    onNextPressed: () -> Unit
) {
//        var darkThemeBoolean by rememberSaveable { mutableStateOf(false) }
//        val darkTheme = DarkTheme(darkThemeBoolean)
//        CompositionLocalProvider(LocalTheme provides darkTheme) {
    var openQuitQuizUpDialog by rememberSaveable { mutableStateOf(false) }
    var openQuitQuizBackDialog by rememberSaveable { mutableStateOf(false) }
    var backPressHandled by rememberSaveable { mutableStateOf(false) }

    BackHandler(enabled = !backPressHandled) {
        openQuitQuizBackDialog = true
        backPressHandled = true
    }

    QuitGameDialog(
        onDismissRequest = {
            openQuitQuizUpDialog = false
            startTimer()
        },
        onConfirmation = {
            openQuitQuizUpDialog = false
            navigateUp()

        },
        shouldShowDialog = openQuitQuizUpDialog,
        pauseTimer = pauseTimer
    )

    val onBackPressedDispatcher =
        LocalOnBackPressedDispatcherOwner.current?.onBackPressedDispatcher
    QuitGameDialog(
        onDismissRequest = {
            openQuitQuizBackDialog = false
            startTimer()
        },
        onConfirmation = {
            openQuitQuizBackDialog = false
            onBackPressedDispatcher?.onBackPressed()
            backPressHandled = false
        },
        shouldShowDialog = openQuitQuizBackDialog,
        pauseTimer = pauseTimer
    )

    Log.d("GameScreen", "Recomposition happens if you update dialog flags")
    if (state.timerState is PausableCountDownTimer.NotStartedTimer) {
        startTimer()
    }

    Log.d(
        "GameScreen",
        "State: $state"
    )

    GameScreen(
        questionIndex = state.currentQuestionIndex,
        questionCount = state.numberOfQuestions,
        onCheckPressed = answerQuestion,
        isCheckEnabled = state.selectedOption !== null && state.quizAnswer == null,
        onNextPressed = onNextPressed,
        isNextEnabled = state.quizAnswer != null,
        navigateUp = { openQuitQuizUpDialog = true },
        onChangeThemePressed = ::onChangeThemePressed,
        answer = state.quizAnswer,
        timerContent = { modifier: Modifier ->
            CountDownTimer(
                timeAvailable = QuizViewModel.INITIAL_TIMER_VALUE,
                timeRemaining = state.timeRemaining,
                modifier = modifier
            )
        }
    ) { innerPadding: PaddingValues ->
        val animatedContentQuestionState = AnimatedContentQuestionState(
            question = state.quizQuestionWithQuizOptions.questionEntity.text,
            options = state.quizQuestionWithQuizOptions.options.map { option ->
                option.text ?: ""
            },
            currentQuestionIndex = state.currentQuestionIndex
        )
        AnimatedContent(
            targetState = animatedContentQuestionState,
            transitionSpec = {
                val animationSpec: TweenSpec<IntOffset> = tween(CONTENT_ANIMATION_DURATION)

                val direction = getTransitionDirection(
                    initialIndex = initialState.currentQuestionIndex,
                    targetIndex = targetState.currentQuestionIndex,
                )

                slideIntoContainer(
                    towards = direction,
                    animationSpec = animationSpec,
                ) togetherWith slideOutOfContainer(
                    towards = direction,
                    animationSpec = animationSpec
                )
            },
            label = "surveyScreenDataAnimation"
        ) {
            QuizQuestion(
                questionText = it.question,
                options = it.options,
                onOptionSelected = onOptionSelected,
                selectedOption = state.selectedOption,
                modifier = Modifier.padding(innerPadding)
            )
        }

    }

//  }
}

private data class AnimatedContentQuestionState(
    val question: String,
    val options: List<String>,
    val currentQuestionIndex: Int
)

private fun onChangeThemePressed() {
    // Get the night mode state of the app.
    val nightMode = AppCompatDelegate.getDefaultNightMode()
    //Set the theme mode for the restarted activity.
    if (nightMode == AppCompatDelegate.MODE_NIGHT_YES) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    } else {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
    }
}

private fun getTransitionDirection(
    initialIndex: Int,
    targetIndex: Int
): AnimatedContentTransitionScope.SlideDirection {
    return if (targetIndex > initialIndex) {
        // Going forwards in the survey: Set the initial offset to start
        // at the size of the content so it slides in from right to left, and
        // slides out from the left of the screen to -fullWidth
        AnimatedContentTransitionScope.SlideDirection.Left
    } else {
        // Going back to the previous question in the set, we do the same
        // transition as above, but with different offsets - the inverse of
        // above, negative fullWidth to enter, and fullWidth to exit.
        AnimatedContentTransitionScope.SlideDirection.Right
    }
}







