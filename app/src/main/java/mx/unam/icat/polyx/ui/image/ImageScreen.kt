package mx.unam.icat.polyx.ui.image

import android.graphics.BitmapFactory
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import com.ortiz.touchview.TouchImageView
import mx.unam.icat.polyx.R
import java.io.IOException

@Composable
fun ImageScreen(
    resourcePath: String,
    onClosePressed: () -> Unit
) {
    Scaffold(
        topBar = {
            ImageScreenTopAppBar(
                onClosePressed = onClosePressed
            )
        }
    ) { contentPadding ->
        Box(
            modifier = Modifier
                .padding(contentPadding)
                .background(Color.Black),
            contentAlignment = Alignment.Center
        ) {
            AndroidView(
                modifier = Modifier.fillMaxSize(),
                factory = {
                    TouchImageView(it)
                        /*.apply {
                            background = ContextCompat.getDrawable(context, R.color.black)
                        }*/
                },
                update = { view ->
                    try {
                        view.setImageBitmap(
                            BitmapFactory.decodeStream(
                                view.context.assets.open(
                                    resourcePath
                                )
                            )
                        )
                    } catch (e: IOException) {
                        Log.d("section_binding_adapter", "Archivo no encontrado")
                    }
                },
            )
        }

    }
}


@Composable
private fun ImageScreenTopAppBar(
    onClosePressed: () -> Unit
) {
    TopAppBar(
        backgroundColor = Color.Black,
        title = { },
        actions = {
            IconButton(
                onClick = onClosePressed
            ) {
                Icon(
                    painter = painterResource(R.drawable.ic_baseline_close_24),
                    contentDescription = "Close",
                    tint = Color.White
                )
            }
        }
    )
}