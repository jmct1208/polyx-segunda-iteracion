package mx.unam.icat.polyx.ui

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.compose.foundation.Image
import androidx.compose.material.AppBarDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp

@Composable
fun PolyxTopBar(
    title: String,
    navigationIcon: Int? = null,
    navigateUp: (() -> Unit)? = null,
    elevation: Dp = AppBarDefaults.TopAppBarElevation
) {
    val navigationIconComposable: @Composable (() -> Unit)? =
        navigationIconComposable(navigationIcon, navigateUp)
    TopAppBar(
        backgroundColor = MaterialTheme.colors.primary,
        title = { Text(text = title) },
        navigationIcon = navigationIconComposable,
        elevation = elevation
    )
}

private fun navigationIconComposable(
    navigationIcon: Int?,
    navigateUp: (() -> Unit)?
): @Composable (() -> Unit)? =
    if (navigationIcon == null || navigateUp == null) {
        null
    } else {
        @Composable {
            IconButton(
                onClick = navigateUp
            ) {
                IconButton(
                    onClick = navigateUp
                ) {
                    Icon(
                        painter = painterResource(navigationIcon),
                        "Go back"
                    )
                }
            }
        }
    }


@Composable
fun AssetImage(
    resourcePath: String,
    modifier: Modifier = Modifier,
    contentDescription: String = "Image from assets"
) {
    var bitmapState by remember { mutableStateOf<Bitmap?>(null) }
    val context = LocalContext.current

    LaunchedEffect(Unit) {
        bitmapState = BitmapFactory.decodeStream(context.assets.open(resourcePath))
    }

    if (bitmapState != null) {
        val bitmap = bitmapState!!.asImageBitmap()
        Image(
            bitmap = bitmap,
            contentDescription = contentDescription,
            modifier = modifier,
            colorFilter = null,
            contentScale = ContentScale.Crop
        )
    }
}