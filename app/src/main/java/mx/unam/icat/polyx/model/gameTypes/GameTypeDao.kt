package mx.unam.icat.polyx.model.gameTypes

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.TypeConverters
import kotlinx.coroutines.flow.Flow

@TypeConverters(GameTypeConverters::class)
@Dao
interface GameTypeDao {
    @Query("SELECT * FROM game_type")
    suspend fun getGameTypes(): List<GameType>

    @Insert
    suspend fun insertGameType(gameType: GameType)
}