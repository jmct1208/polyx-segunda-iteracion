package mx.unam.icat.polyx.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.compositionLocalOf

val LightColors = lightColors(
    primary = LightBlue500,
    primaryVariant = LightBlue700,
    onPrimary = Black,
    secondary = Orange500,
    secondaryVariant = Orange700
)

val DarkColors = darkColors(
    primary = LightBlue100,
    primaryVariant = LightBlue500,
    secondary = Orange100,
    secondaryVariant = Orange100
)

data class DarkTheme(val isDark: Boolean = false)

val LocalTheme = compositionLocalOf { DarkTheme() }

@Composable
fun PolyxTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit,
) {
    val colors = if (darkTheme) DarkColors else LightColors

    MaterialTheme(
        colors = colors,
        typography = PolyxTypography,
        content = content
    )
}

