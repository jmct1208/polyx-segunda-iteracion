package mx.unam.icat.polyx

import kotlinx.serialization.Serializable

@Serializable data class CourseRoute(val courseName: String)

@Serializable
data class GameTypeUnitsRoute(val courseId: Long, val gameTypeName: String)

@Serializable
data class UnitRoute(val unitId: Long, val initialScrollSectionId: Long)

@Serializable
data class GameRoute(val unitId: Long)

@Serializable
data object QuizRoute

@Serializable
data object QuizResultsRoute

@Serializable data class FillInTheBlankRoute(val unitId: Long)

@Serializable data class ImageRoute(val imageUrl: String)