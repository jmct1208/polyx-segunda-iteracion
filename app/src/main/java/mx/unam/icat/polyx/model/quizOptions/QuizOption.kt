package mx.unam.icat.polyx.model.quizOptions

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "quiz_option", indices = [Index(value = ["text", "is_answer"], unique = true)])
data class QuizOption(
    @ColumnInfo(name = "is_answer") val isAnswer: Boolean,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "quiz_option_id")
    val quizOptionId: Long,
    @ColumnInfo(name = "resource_path")
    val resourcePath: String? = null,
    val text: String? = null
)

sealed interface BaseQuizOption {
    val quizOptionId: Long
    val isAnswer: Boolean
}

data class TextQuizOption(
    override val quizOptionId: Long = 0,
    override val isAnswer: Boolean,
    val text: String
) :
    BaseQuizOption

data class ImageQuizOption(
    override val quizOptionId: Long = 0,
    override val isAnswer: Boolean,
    val resourcePath: String
) :
    BaseQuizOption

fun QuizOption.toUiState(): BaseQuizOption {
    return if (resourcePath == null && text == null) throw IllegalStateException()
    else if (resourcePath == null) TextQuizOption(quizOptionId, isAnswer, text!!)
    else ImageQuizOption(quizOptionId, isAnswer, resourcePath)
}

fun TextQuizOption.toModel() = QuizOption(isAnswer, quizOptionId, text = text)
fun ImageQuizOption.toModel() = QuizOption(isAnswer, quizOptionId, resourcePath = resourcePath)
fun BaseQuizOption.toModel() = when(this) {
    is TextQuizOption -> toModel()
    is ImageQuizOption -> toModel()
}