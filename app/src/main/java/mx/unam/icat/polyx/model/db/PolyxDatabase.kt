package mx.unam.icat.polyx.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import mx.unam.icat.polyx.model.gameTypes.GameType
import mx.unam.icat.polyx.model.gameTypes.GameTypeDao
import mx.unam.icat.polyx.model.questions.QuestionEntity
import mx.unam.icat.polyx.model.questions.QuizQuestionDao
import mx.unam.icat.polyx.model.quizOptions.QuizQuestionQuizOption
import mx.unam.icat.polyx.model.quizOptions.QuizOption
import mx.unam.icat.polyx.model.quizOptions.QuizOptionDao
import mx.unam.icat.polyx.model.section.SectionEntity
import mx.unam.icat.polyx.model.section.SectionDao
import mx.unam.icat.polyx.model.fillintheblanksentence.FillInTheBlankSentence
import mx.unam.icat.polyx.model.fillintheblanksentence.FillInTheBlankSentenceDao
import mx.unam.icat.polyx.model.fillintheblankcorrectoption.FillInTheBlankCorrectOption
import mx.unam.icat.polyx.model.fillintheblankincorrectoption.FillInTheBlankSentenceFillInTheBlankIncorrectOption
import mx.unam.icat.polyx.model.fillintheblankincorrectoption.FillInTheBlankIncorrectOption

@Database(
    entities = [GameType::class, QuestionEntity::class, QuizOption::class,
        QuizQuestionQuizOption::class, SectionEntity::class, FillInTheBlankSentence::class, FillInTheBlankCorrectOption::class,
        FillInTheBlankIncorrectOption::class, FillInTheBlankSentenceFillInTheBlankIncorrectOption::class],
    version = 1
)
abstract class PolyxDatabase : RoomDatabase() {

    abstract fun gameTypeDao(): GameTypeDao

    abstract fun questionDao(): QuizQuestionDao

    abstract fun sectionDao(): SectionDao

    abstract fun quizOptionDao(): QuizOptionDao

    abstract fun sentenceDao(): FillInTheBlankSentenceDao
}