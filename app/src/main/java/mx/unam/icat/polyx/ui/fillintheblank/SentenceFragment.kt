package mx.unam.icat.polyx.ui.fillintheblank

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import mx.unam.icat.polyx.R
import mx.unam.icat.polyx.databinding.FragmentFillInTheBlankBinding

@AndroidEntryPoint
class SentenceFragment : Fragment() {
//    val fillInTheBlankViewModel by hiltNavGraphViewModels<FillInTheBlankViewModel>(R.id.fill_in_the_blank_graph)
    private var binding: FragmentFillInTheBlankBinding? = null
    private val sentenceSegmentCardviewIds = mutableListOf<Int>()
    private val optionCardviewIds = mutableListOf<Int>()
    private val blankSlotViewIds = listOf<Int>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFillInTheBlankBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            toolbar.apply {
                navigationIcon = AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.ic_baseline_arrow_back_24
                )
                setNavigationOnClickListener {
                    if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
                        findNavController().navigateUp()
                }
            }
        }
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewSubscriptions()
    }

    private fun viewSubscriptions() {
        /*fillInTheBlankViewModel.fillInTheBlankUiState.observe(viewLifecycleOwner) {
            when (it) {
                is FillInTheBlankUiState.Loading -> {}
                is FillInTheBlankUiState.Success -> {
                    Log.d(SentenceFragment::class.simpleName, "$it")
                    binding!!.sentence.text = it.sentence + "\n"
                    binding!!.sentence.blankIndicesList = it.blankStartEndIndices
                }
                is FillInTheBlankUiState.GameOver -> {}
                else -> {}
            }
        }*/
    }


    companion object {
        val TAG = SentenceFragment::class.simpleName
    }
}