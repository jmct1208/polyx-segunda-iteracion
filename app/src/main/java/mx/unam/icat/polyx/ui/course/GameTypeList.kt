package mx.unam.icat.polyx.ui.course

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import mx.unam.icat.polyx.home.presentation.GameTypeCard
import mx.unam.icat.polyx.home.presentation.HorizontalThumbnailAndTextCardDivider
import mx.unam.icat.polyx.model.gameTypes.GameType

@Composable
fun GameTypeList(gameTypeList: List<GameType>, navigateToGameTypeUnitsScreenByCourseId: (GameType) -> Unit) {
    LazyColumn(Modifier.fillMaxSize()) {
        itemsIndexed(gameTypeList) { index, gameType ->
            GameTypeCard(gameType) { navigateToGameTypeUnitsScreenByCourseId(it) }
            if (index < gameTypeList.lastIndex) { HorizontalThumbnailAndTextCardDivider() }
        }
    }
}