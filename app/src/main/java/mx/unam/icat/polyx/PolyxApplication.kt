package mx.unam.icat.polyx

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PolyxApplication: Application()