package mx.unam.icat.polyx.model.db

import android.content.Context
import android.util.Log
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import mx.unam.icat.polyx.model.fillintheblanksentence.FillInTheBlankSentenceDao
import mx.unam.icat.polyx.model.gameTypes.GameTypeDao
import mx.unam.icat.polyx.model.questions.QuizQuestionDao
import mx.unam.icat.polyx.model.quizOptions.QuizOptionDao
import mx.unam.icat.polyx.model.section.SectionDao
import java.util.concurrent.Executors
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {
    @Provides
    fun provideGameTypeDao(database: PolyxDatabase): GameTypeDao {
        return database.gameTypeDao()
    }

    @Provides
    fun provideQuestionDao(database: PolyxDatabase): QuizQuestionDao {
        return database.questionDao()
    }

    @Provides
    fun provideSectionDao(database: PolyxDatabase): SectionDao {
        return database.sectionDao()
    }

    @Provides
    fun provideQuizOptionDao(database: PolyxDatabase): QuizOptionDao {
        return database.quizOptionDao()
    }

    @Provides
    fun provideSentenceDao(database: PolyxDatabase): FillInTheBlankSentenceDao {
        return database.sentenceDao()
    }

    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext applicationContext: Context,
    ): PolyxDatabase {
        val dbBuilder = Room.databaseBuilder(
            applicationContext,
            PolyxDatabase::class.java,
            "polyx_database"
        ).createFromAsset("polyx.db")
            /*.setQueryCallback(RoomDatabase.QueryCallback { sqlQuery, bindArgs ->
                val i = 3000
                var sb = sqlQuery
                while (sb.length > i) {
                    Log.e(DatabaseModule::javaClass.name, "Substring: " + sb.substring(0, i))
                    sb = sb.substring(i)
                }
                Log.e(DatabaseModule::javaClass.name, sb)
                Log.e(DatabaseModule::javaClass.name, "SQL Args: $bindArgs")
            }, Executors.newSingleThreadExecutor())*/
            .fallbackToDestructiveMigration()
        return dbBuilder.build()
    }
}
