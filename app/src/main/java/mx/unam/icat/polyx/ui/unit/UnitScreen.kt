package mx.unam.icat.polyx.ui.unit

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import mx.unam.icat.polyx.R
import mx.unam.icat.polyx.model.section.SectionEntity
import mx.unam.icat.polyx.ui.PolyxTopBar


@Composable
fun SectionList(
    sectionList: List<Pair<SectionEntity, Int>>,
    modifier: Modifier = Modifier,
    navigateToImageDialog: (String) -> Unit,
    sectionListState: LazyListState
) {
    LazyColumn(
        state = sectionListState,
        modifier = modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(8.dp),
        contentPadding = PaddingValues(16.dp)
    ) {
        items(sectionList) { pair ->
            when (pair.first.type) {
                SectionEntity.Type.TITLE -> SelectableTitle(pair.first.title1, pair.second)
                SectionEntity.Type.TEXT -> TextContent(pair.first.textContent1)
                SectionEntity.Type.IMAGE -> ImageSection(
                    resource = pair.first.resourcePath1,
                    onClick = navigateToImageDialog
                )

                SectionEntity.Type.TWO_IMAGES -> TwoImagesSection(
                    resource1 = pair.first.resourcePath1,
                    resource2 = pair.first.resourcePath2,
                    onClick1 = navigateToImageDialog,
                    onClick2 = navigateToImageDialog
                )

                SectionEntity.Type.TWO_SUBTITLES_TWO_IMAGES -> TwoSubtitlesTwoImagesSection(
                    title1 = pair.first.title1,
                    title2 = pair.first.title2,
                    resource1 = pair.first.resourcePath1,
                    resource2 = pair.first.resourcePath2,
                    onClick1 = navigateToImageDialog,
                    onClick2 = navigateToImageDialog
                )

                SectionEntity.Type.TWO_SUBTITLES_TWO_TEXTS -> TwoSubtitlesTwoTextsSection(
                    title1 = pair.first.title1,
                    title2 = pair.first.title2,
                    text1 = pair.first.textContent1,
                    text2 = pair.first.textContent2
                )

                SectionEntity.Type.TWO_SUBTITLES_TWO_TEXTS_TWO_IMAGES -> TwoSubtitlesTwoTextsTwoImages(
                    title1 = pair.first.title1,
                    title2 = pair.first.title2,
                    textContent1 = pair.first.textContent1,
                    textContent2 = pair.first.textContent2,
                    resource1 = pair.first.resourcePath1,
                    resource2 = pair.first.resourcePath2,
                    onClick1 = navigateToImageDialog,
                    onClick2 = navigateToImageDialog
                )

                SectionEntity.Type.TWO_SUBTITLES_TWO_IMAGES_TWO_TEXTS -> TwoSubtitlesTwoImagesTwoTexts(
                    title1 = pair.first.title1,
                    title2 = pair.first.title2,
                    textContent1 = pair.first.textContent1,
                    textContent2 = pair.first.textContent2,
                    resource1 = pair.first.resourcePath1,
                    resource2 = pair.first.resourcePath2,
                    onClick1 = navigateToImageDialog,
                    onClick2 = navigateToImageDialog
                )

                SectionEntity.Type.IMAGE_TITLE -> throw IllegalStateException()
            }
        }
    }
}

@Composable
fun SuccessUnitScreen(
    sectionList: List<Pair<SectionEntity, Int>>,
    unitTitle: String,
    initialScrollToItemId: Long?,
    navigateUp: () -> Unit,
    navigateToImageDialog: (String) -> Unit
) {
    val sectionListState = rememberLazyListState()
    LaunchedEffect(initialScrollToItemId) {
        if (initialScrollToItemId == null) return@LaunchedEffect
        val indexOfSection =
            sectionList.indexOfFirst { it.first.sectionId == initialScrollToItemId }
        sectionListState.animateScrollToItem(indexOfSection)
    }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            PolyxTopBar(
                title = unitTitle,
                navigationIcon = R.drawable.ic_baseline_arrow_back_24,
                navigateUp = navigateUp
            )
        }
    ) { innerPadding: PaddingValues ->
        SectionList(
            sectionList = sectionList,
            modifier = Modifier.padding(innerPadding),
            sectionListState = sectionListState,
            navigateToImageDialog = navigateToImageDialog
        )

    }
}

@Composable
fun UnitScreen(
    unitViewModel: UnitViewModel = hiltViewModel(),
    navigateUp: () -> Unit,
    navigateToImageDialog: (String) -> Unit
) {
    val unitUiState by unitViewModel.unitUiState.collectAsState()
    when (val state = unitUiState) {
        is UnitUiState.Loading -> {}
        is UnitUiState.Success -> {
            Log.d(
                "UnitScreen",
                "Scroll to section id: ${state.initialScrollToItemId}"
            )
            SuccessUnitScreen(
                sectionList = state.sections,
                unitTitle = stringResource(
                    R.string.unit,
                    state.sections[0].first.childOrder
                ),
                navigateUp = navigateUp,
                navigateToImageDialog = navigateToImageDialog,
                initialScrollToItemId = state.initialScrollToItemId,
            )
        }
    }
}
