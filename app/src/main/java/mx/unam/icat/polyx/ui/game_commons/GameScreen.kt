package mx.unam.icat.polyx.ui.game_commons

import android.content.res.Configuration
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInVertically
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.WindowInsetsSides
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.only
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBars
import androidx.compose.foundation.layout.windowInsetsPadding
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Face
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import mx.unam.icat.polyx.R
import mx.unam.icat.polyx.ui.quiz.CorrectQuizAnswer
import mx.unam.icat.polyx.ui.quiz.IncorrectQuizAnswer
import mx.unam.icat.polyx.ui.quiz.NoQuizAnswer
import mx.unam.icat.polyx.ui.quiz.QuizAnswer
import mx.unam.icat.polyx.ui.theme.PolyxTheme
import mx.unam.icat.polyx.ui.theme.correctAnswerGreen
import mx.unam.icat.polyx.ui.theme.incorrectAnswerRed
import mx.unam.icat.polyx.ui.theme.noAnswerYellow

@Composable
fun GameScreen(
    questionIndex: Int,
    questionCount: Int,
    onCheckPressed: () -> Unit,
    isCheckEnabled: Boolean,
    onNextPressed: () -> Unit,
    isNextEnabled: Boolean,
    navigateUp: () -> Unit,
    onChangeThemePressed: () -> Unit,
    answer: QuizAnswer?,
    timerContent: @Composable ((modifier: Modifier) -> Unit)? = null,
    content: @Composable (PaddingValues) -> Unit,
) {
    Scaffold(
        topBar = {
            GameTopAppBar(
                questionIndex = questionIndex,
                questionCount = questionCount,
                onClosePressed = navigateUp,
                onChangeThemePressed = onChangeThemePressed
            )
        },

        content = content,
        bottomBar = {
            GameBottomAppBar(
                onCheckPressed = onCheckPressed,
                isCheckEnabled = isCheckEnabled,
                onNextPressed = onNextPressed,
                isNextEnabled = isNextEnabled,
                timer = timerContent,
                answer = answer
            )
        }
    )
}

@Composable
fun GameTopAppBar(
    questionIndex: Int,
    questionCount: Int,
    onClosePressed: () -> Unit,
    onChangeThemePressed: () -> Unit
) {
    TopAppBar(
        title = {
            Text(text = stringResource(R.string.question_progress, questionIndex, questionCount))
        },
        navigationIcon = {
            IconButton(
                onClick = onClosePressed,
                modifier = Modifier.padding(4.dp)
            ) {
                Icon(
                    imageVector = Icons.Filled.Close,
                    contentDescription = stringResource(id = R.string.close),
                    tint = MaterialTheme.colors.onSurface
                )
            }
        },
        actions = {
            IconButton(
                onClick = onChangeThemePressed
            ) {
                Icon(
                    imageVector = Icons.Filled.Face,
                    contentDescription = "Change theme",
                    tint = MaterialTheme.colors.onSurface
                )
            }
        }
    )
}

@Composable
fun GameBottomAppBar(
    onNextPressed: () -> Unit,
    onCheckPressed: () -> Unit,
    isCheckEnabled: Boolean,
    isNextEnabled: Boolean,
    answer: QuizAnswer?,
    timer: (@Composable (modifier: Modifier) -> Unit)? = null
) {
    Box {
        CheckAndTimerSurface(
            onCheckPressed = onCheckPressed,
            isCheckEnabled = isCheckEnabled,
            modifier = Modifier.align(Alignment.BottomCenter),
            timer = timer
        )
        val visible = answer != null
        val density = LocalDensity.current
        AnimatedVisibility(
            visible = visible,
            enter = slideInVertically { fullHeight ->
                with(density) {
                    (5 * fullHeight).dp.roundToPx()
                }
            },
            modifier = Modifier.align(Alignment.BottomCenter)
        ) {
            answer?.let {
                AnswerChooser(
                    onNextPressed = onNextPressed,
                    isNextEnabled = isNextEnabled,
                    quizAnswer = it
                )
            }

        }
    }
}

@Composable
fun CheckAndTimerSurface(
    onCheckPressed: () -> Unit,
    isCheckEnabled: Boolean,
    modifier: Modifier = Modifier,
    timer: (@Composable (modifier: Modifier) -> Unit)? = null
) {
    Surface(
        elevation = 7.dp,
        modifier = modifier.fillMaxWidth()
    ) {
        Column(
            modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 8.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            if (timer != null) {
                timer(Modifier.align(Alignment.CenterHorizontally))
            }
            Button(
                modifier = Modifier
                    .windowInsetsPadding(
                        WindowInsets.systemBars.only(
                            WindowInsetsSides.Horizontal + WindowInsetsSides.Vertical
                        )
                    )
                    .align(Alignment.CenterHorizontally)
                    .fillMaxWidth(),
                onClick = onCheckPressed,
                enabled = isCheckEnabled
            ) {
                Text(text = stringResource(id = R.string._continue))
            }
        }
    }
}

@Composable
fun AnswerChooser(
    quizAnswer: QuizAnswer,
    onNextPressed: () -> Unit,
    isNextEnabled: Boolean
) {
    when (quizAnswer) {
        is CorrectQuizAnswer -> AnswerSurface(
            onNextPressed = onNextPressed,
            isNextEnabled = isNextEnabled,
            color = correctAnswerGreen,
            title = stringResource(R.string.correct_answer_dialog_title)
        )

        is IncorrectQuizAnswer -> AnswerSurface(
            onNextPressed,
            isNextEnabled,
            incorrectAnswerRed,
            stringResource(R.string.incorrect_answer_dialog_title),
            stringResource(R.string.the_correct_answer, quizAnswer.correctOption)
        )

        is NoQuizAnswer -> AnswerSurface(
            onNextPressed,
            isNextEnabled,
            noAnswerYellow,
            stringResource(R.string.no_answer_dialog_title),
            stringResource(R.string.the_correct_answer, quizAnswer.correctOption)
        )
    }
}

//Create a composable preview of the answer surface with a correct answer
@Composable
@Preview(showBackground = true)
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
fun CorrectAnswerPreview() {
    PolyxTheme {
        AnswerChooser(
            quizAnswer = quizAnswer1,
            onNextPressed = {},
            isNextEnabled = true
        )
    }
}

//Create a composable preview of the answer surface with an incorrect answer
@Composable
@Preview(showBackground = true)
fun IncorrectAnswerPreview() {
    PolyxTheme {
        AnswerChooser(
            quizAnswer = quizAnswer,
            onNextPressed = {},
            isNextEnabled = true
        )
    }
}

//Create a composable preview of the answer surface with no answer
@Composable
@Preview
fun NoAnswerPreview() {
    PolyxTheme {
        AnswerChooser(
            quizAnswer = quizAnswer2,
            onNextPressed = {},
            isNextEnabled = false
        )
    }
}


@Composable
fun AnswerSurface(
    onNextPressed: () -> Unit,
    isNextEnabled: Boolean,
    color: Color,
    title: String,
    message: String? = null
) {
    Surface(
        elevation = 8.dp,
        color = color,
    ) {
        Column(
            modifier = Modifier.padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            Text(
                text = title,
                style = MaterialTheme.typography.h6,
                modifier = Modifier.fillMaxWidth()
            )
            if (message != null) {
                Text(text = message)
            }
            Button(
                onClick = onNextPressed,
                enabled = isNextEnabled,
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .fillMaxWidth()
                    .windowInsetsPadding(
                        WindowInsets.systemBars.only(
                            WindowInsetsSides.Horizontal + WindowInsetsSides.Vertical
                        )
                    )
            ) {
                Text(text = stringResource(R.string._continue))
            }
        }
    }
}

@Composable
fun QuitGameDialog(
    onDismissRequest: () -> Unit,
    onConfirmation: () -> Unit,
    shouldShowDialog: Boolean,
    pauseTimer: () -> Unit,
) {
    if (shouldShowDialog) {
        pauseTimer()
        AlertDialog(
            title = { Text(text = stringResource(R.string.quit_quiz_confirmation)) },
            text = { Text(text = stringResource(R.string.quit_quiz_warning)) },
            onDismissRequest = onDismissRequest,
            confirmButton = {
                TextButton(
                    onClick = onConfirmation
                ) {
                    Text(text = stringResource(R.string.exit))
                }
            },
            dismissButton = {
                TextButton(
                    onClick = onDismissRequest
                ) {
                    Text(text = stringResource(R.string.cancel))
                }
            }
        )
    }

}

data class GameData(
    val questionIndex: Int,
    val questionCount: Int
)

val quizAnswer = IncorrectQuizAnswer(
    question = "What is the capital of Mexico?",
    correctOption = "Mexico City",
    selectedOption = "New York",
    sectionOwnerId = 31,
    unitId = 1,
    closestParentSectionId = 2,
)

val quizAnswer1 = CorrectQuizAnswer(
    question = "What is the capital of Mexico?",
    correctOption = "Mexico City"
)

val quizAnswer2 = NoQuizAnswer(
    question = "What is the capital of Mexico?",
    correctOption = "Mexico City",
    sectionOwnerId = 31,
    unitId = 1,
    closestParentSectionId = 2,
)
