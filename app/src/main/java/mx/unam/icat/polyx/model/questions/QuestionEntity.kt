package mx.unam.icat.polyx.model.questions

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import mx.unam.icat.polyx.model.section.SectionEntity

@Entity(
    tableName = "quiz_question",
    foreignKeys = [ForeignKey(
        entity = SectionEntity::class,
        parentColumns = ["section_id"],
        childColumns = ["section_owner_id"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.SET_NULL
    )]
)

data class QuestionEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "quiz_question_id")
    val questionId: Long,
    @ColumnInfo(name = "section_owner_id", index = true) val sectionOwnerId: Long?,
    val text: String,
    @ColumnInfo(name = "resource_path") val resourcePath: String?
)