package mx.unam.icat.polyx.ui.game_commons

import mx.unam.icat.polyx.model.questions.QuestionEntity
import mx.unam.icat.polyx.model.quizOptions.QuizOption

val question1 = QuestionEntity(
    questionId = 11,
    sectionOwnerId = 1,
    text = "Las proteinas son:",
    resourcePath = null
)

val option1 = QuizOption(
    isAnswer = true,
    quizOptionId = 38,
    resourcePath = null,
    text = "Polímeros naturales"
)
val option2 = QuizOption(
    isAnswer = false,
    quizOptionId = 39,
    resourcePath = null,
    text = "No son polímeros"
)
val option3 = QuizOption(
    isAnswer = false,
    quizOptionId = 40,
    resourcePath = null,
    text = "Materiales biodegradables"
)
val option4 = QuizOption(
    isAnswer = false,
    quizOptionId = 41,
    resourcePath = null,
    text = "Polímeros regenerados"
)
val options1 = listOf(
    option1, option2, option3, option4
)

