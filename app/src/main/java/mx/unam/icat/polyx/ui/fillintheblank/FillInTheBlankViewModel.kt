package mx.unam.icat.polyx.ui.fillintheblank

import android.util.Log
import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import mx.unam.icat.polyx.model.fillintheblanksentence.SentenceRepository
import mx.unam.icat.polyx.model.fillintheblanksentence.SentenceWithOptionsAndSections
import mx.unam.icat.polyx.model.section.SectionRepository
import javax.inject.Inject

@HiltViewModel
class FillInTheBlankViewModel @Inject constructor(
    sentenceRepository: SentenceRepository,
    sectionRepository: SectionRepository,
    state: SavedStateHandle
) : ViewModel() {
    private val unitId = state.get<Long>("unitId")!!
    private val sentenceList = mutableListOf<SentenceWithOptionsAndSections>()
    private var currentIndex = 0
    private val blankChar = 'x'
    private var currentIndexBlankString: String? = null
    private var currentOptionList: List<String>? = null

    private val sentencesFlow =
        sentenceRepository.getSentenceWithOptionsAndSectionsByUnitId(unitId)
    private val _answerUiState = MutableStateFlow<List<FillInTheBlankAnswer>>(listOf())
    private val _fillInTheBlankUiState =
        MutableStateFlow<FillInTheBlankUiState>(FillInTheBlankUiState.Loading)

    private val studySectionId = MutableLiveData<Long?>(null)
    val fillInTheBlankUiState = _fillInTheBlankUiState.asLiveData()
    val answerUiState = _answerUiState.asLiveData()
    @OptIn(ExperimentalCoroutinesApi::class)
    val sentenceText = _fillInTheBlankUiState.mapLatest {
        when (it) {
            is FillInTheBlankUiState.Success -> it.sentence
            else -> null
        }
    }.distinctUntilChanged().asLiveData()
    @OptIn(ExperimentalCoroutinesApi::class)
    val optionsAndTransition = _fillInTheBlankUiState.mapLatest {
        when (it) {
            is FillInTheBlankUiState.Success -> Triple(
                it.blankSlots,
                it.transition,
                it.optionStrings
            )
            else -> null
        }
    }.distinctUntilChanged().asLiveData()


    init {
        viewModelScope.launch {
            sentencesFlow.collect {
                if (it.isEmpty() && sentenceList.isEmpty())
                    _fillInTheBlankUiState.update { FillInTheBlankUiState.GameOver }
                else if (sentenceList.isEmpty() && it.isNotEmpty()) {
                    sentenceList.addAll(it)
                    _fillInTheBlankUiState.update {
                        sentenceList[0].toSuccess()
                    }
                } else if (sentenceList.isNotEmpty())
                    sentenceList.addAll(it.minus(sentenceList.toSet()).shuffled())
            }
        }
    }

    fun setStudySectionId(id: Long?) {
        studySectionId.value = id
    }

    private fun SentenceWithOptionsAndSections.toSuccess(): FillInTheBlankUiState.Success {
        val correctOptionStrings = mutableListOf<String>()
        fillInTheBlankCorrectOptions.forEach {
            val string = this@toSuccess.fillInTheBlankSentence.text.substring(
                it.position,
                it.position + it.length
            )
            val stringList = string.split("\\s+".toRegex())
            correctOptionStrings.addAll(stringList)
        }
        currentOptionList = buildList {
            addAll(correctOptionStrings)
            addAll(fillInTheBlankIncorrectOptions.map { it.text })
            shuffle()
        }
        Log.d(FillInTheBlankViewModel::class.simpleName, currentOptionList!!.maxBy { it.length })
        val length = currentOptionList!!.maxBy { it.length }.length
        currentIndexBlankString =
            blankChar.toString().repeat(length)
        var sentenceNoDuplicateWhiteSpace = fillInTheBlankSentence.text
            .replace("\\s+".toRegex(), " ")
        val blankStartIndices = mutableListOf<Pair<Int, Int>>()

        correctOptionStrings.forEach {
            val startIndex = sentenceNoDuplicateWhiteSpace.indexOf(it)
            val endIndex = startIndex + length - 1
            blankStartIndices.add(Pair(startIndex, endIndex))
            sentenceNoDuplicateWhiteSpace = sentenceNoDuplicateWhiteSpace
                .replaceFirst(it, "$currentIndexBlankString")
        }
        return FillInTheBlankUiState.Success(
            sentenceNoDuplicateWhiteSpace,
            blankStartIndices,
            currentOptionList!!,
            List(correctOptionStrings.size) { null },
            null,
            null
        )
    }

    fun changeToNextSentence() {
        currentIndex++
        try {
            _fillInTheBlankUiState.update {
                when (it) {
                    is FillInTheBlankUiState.Success -> {
                        sentenceList[currentIndex].toSuccess()
                    }
                    else -> it
                }
            }
        } catch (e: IndexOutOfBoundsException) {
            _fillInTheBlankUiState.update { FillInTheBlankUiState.GameOver }
        }
    }

    fun answerQuestion() {
        _fillInTheBlankUiState.update {
            if (it is FillInTheBlankUiState.Success && it.fillInTheBlankAnswer == null)
                it.copy(fillInTheBlankAnswer = getAnswerType(it.sentence, it.blankSlots))
            else it
        }
    }

    private fun getAnswerType(
        sentenceWithBlanks: String,
        blankSlots: List<String?>
    ): FillInTheBlankAnswer {
        var userString = sentenceWithBlanks
        blankSlots.forEach {
            if (it != null)
                userString = userString.replaceFirst(currentIndexBlankString!!, it)
        }
        val validSentence = sentenceList[currentIndex].fillInTheBlankSentence.text
        Log.d(FillInTheBlankViewModel::class.simpleName, "User string: $userString")
        val answer: FillInTheBlankAnswer =
            if (userString == validSentence)
                FillInTheBlankAnswer.CorrectAnswer(validSentence)
            else FillInTheBlankAnswer.IncorrectAnswer(userString, validSentence)

        _answerUiState.update {
            buildList {
                addAll(it)
                add(answer)
            }
        }
        return answer
    }

    fun blankClicked(position: Int) {
        _fillInTheBlankUiState.update {
            if (it is FillInTheBlankUiState.Success && it.fillInTheBlankAnswer == null) {
                val optionClicked = it.blankSlots[position]!!
                val toIndex = currentOptionList!!.indexOf(optionClicked)
                it.copy(transition = Transition(false, position, toIndex))
            } else it
        }
    }

    fun optionClicked(position: Int) {
        _fillInTheBlankUiState.update {
            if (it is FillInTheBlankUiState.Success && it.fillInTheBlankAnswer == null) {
                val indexOfFirstAvailableBlank = it.blankSlots.indexOfFirst { s: String? ->
                    s == null
                }
                val transition = Transition(true, position, indexOfFirstAvailableBlank)
                it.copy(transition = transition)
            } else it
        }
    }

    fun animationFinished() {
        _fillInTheBlankUiState.update {
            if (it is FillInTheBlankUiState.Success && it.fillInTheBlankAnswer == null
                && it.transition != null
            ) {
                if (it.transition.direction) {
                    val newOptionList = it.optionStrings.toMutableList()
                    val option = newOptionList[it.transition.fromIndex]
                    newOptionList[it.transition.fromIndex] = null
                    val newBlankSlots = it.blankSlots.toMutableList()
                    newBlankSlots[it.transition.toIndex] = option
                    it.copy(optionStrings = newOptionList, blankSlots = newBlankSlots, transition = null)
                } else {
                    val newBlankSlots = it.blankSlots.toMutableList()
                    val option = newBlankSlots[it.transition.fromIndex]
                    newBlankSlots[it.transition.fromIndex] = null
                    val newOptionStrings = it.optionStrings.toMutableList()
                    newOptionStrings[it.transition.toIndex] = option
                    it.copy(optionStrings = newOptionStrings, blankSlots = newBlankSlots, transition = null)
                }
            } else it
        }
    }
}

sealed interface FillInTheBlankAnswer  {
    data class CorrectAnswer(val validSentence: String) : FillInTheBlankAnswer
    data class IncorrectAnswer(val invalidSentence: String, val validSentence: String) :
        FillInTheBlankAnswer
}

sealed interface FillInTheBlankUiState {
    object Loading : FillInTheBlankUiState
    object GameOver : FillInTheBlankUiState
    data class Success(
        val sentence: String,
        val blankStartEndIndices: List<Pair<Int, Int>>,
        val optionStrings: List<String?>,
        val blankSlots: List<String?>,
        val fillInTheBlankAnswer: FillInTheBlankAnswer?,
        val transition: Transition?
    ) : FillInTheBlankUiState
}

//true - from option part to blanks part
//false - from blanks part to option part
data class Transition(
    val direction: Boolean,
    val fromIndex: Int,
    val toIndex: Int
)