package mx.unam.icat.polyx.model.fillintheblanksentence

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import kotlinx.coroutines.flow.Flow

@Dao
interface FillInTheBlankSentenceDao {
    @Transaction
    @Query(
        "WITH RECURSIVE under_section AS (" +
                "SELECT s0.*, 0 as depth FROM section s0  WHERE section_id = :unitId " +
                "UNION ALL " +
                "SELECT s1.*, u_s.depth + 1 " +
                "FROM section AS s1 JOIN under_section AS u_s  ON s1.section_owner_id=u_s.section_id " +
                "ORDER BY 11 DESC, 3) " +
                "SELECT DISTINCT s.* FROM under_section u INNER JOIN fill_in_the_blank_sentence s ON s.section_owner_id = u.section_id"
    )
    fun getSentencesWithOptionsAndSectionsByUnitId(unitId: Long): Flow<List<SentenceWithOptionsAndSections>>
}