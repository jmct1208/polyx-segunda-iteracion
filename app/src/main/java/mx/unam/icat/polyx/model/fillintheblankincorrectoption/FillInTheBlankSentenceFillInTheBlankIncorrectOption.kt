package mx.unam.icat.polyx.model.fillintheblankincorrectoption

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import mx.unam.icat.polyx.model.fillintheblanksentence.FillInTheBlankSentence

@Entity(
    tableName = "fill_in_the_blank_sentence_fill_in_the_blank_incorrect_option",
    foreignKeys = [ForeignKey(
        entity = FillInTheBlankSentence::class,
        parentColumns = ["fill_in_the_blank_sentence_id"],
        childColumns = ["fill_in_the_blank_sentence_owner_id"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    ), ForeignKey(
        entity = FillInTheBlankIncorrectOption::class,
        parentColumns = ["fill_in_the_blank_incorrect_option_id"],
        childColumns = ["fill_in_the_blank_incorrect_option_owner_id"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )],
    primaryKeys = ["fill_in_the_blank_sentence_owner_id", "fill_in_the_blank_incorrect_option_owner_id"]
)
data class FillInTheBlankSentenceFillInTheBlankIncorrectOption(
    @ColumnInfo(name = "fill_in_the_blank_sentence_owner_id", index = true)
    val fillInTheBlankSentenceOwnerId: Long,
    @ColumnInfo(name = "fill_in_the_blank_incorrect_option_owner_id", index = true)
    val fillInTheBlankIncorrectOptionOwnerId: Long
)