package mx.unam.icat.polyx.model.questions

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import mx.unam.icat.polyx.model.quizOptions.QuizQuestionQuizOption
import mx.unam.icat.polyx.model.quizOptions.QuizOption

data class QuizQuestionWithQuizOptions(
    @Embedded var questionEntity: QuestionEntity,
    @Relation(
        parentColumn = "quiz_question_id",
        entityColumn = "quiz_option_id",
        entity = QuizOption::class,
        associateBy = Junction(
            value = QuizQuestionQuizOption::class,
            parentColumn = "quiz_question_owner_id",
            entityColumn = "quiz_option_owner_id"
        )
    )
    var options: List<QuizOption>
)