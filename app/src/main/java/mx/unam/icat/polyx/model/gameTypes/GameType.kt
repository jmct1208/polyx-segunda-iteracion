package mx.unam.icat.polyx.model.gameTypes

import androidx.room.*

@Entity(tableName = "game_type")
data class GameType(
    val name: GameTypeEnum,
    @ColumnInfo(name = "resource_path")
    val resourcePath: String,
    @ColumnInfo(name = "game_type_id")
    @PrimaryKey(autoGenerate = true) val id: Long = 0
)

class GameTypeConverters {
    @TypeConverter
    fun fromGameTypeEnum(gameType: GameTypeEnum) = gameType.name

    @TypeConverter
    fun fromString(gameTypeName: String) = GameTypeEnum.valueOf(gameTypeName)
}

enum class GameTypeEnum {
    QUIZ, FILL_IN_THE_BLANK, MATCH_COLUMNS
}


