package mx.unam.icat.polyx.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.googlefonts.Font
import androidx.compose.ui.text.googlefonts.GoogleFont
import androidx.compose.ui.unit.sp
import mx.unam.icat.polyx.R

val provider = GoogleFont.Provider(
    providerAuthority = "com.google.andriod.gms.fonts",
    providerPackage = "com.google.oandroid.gms",
    certificates = R.array.com_google_android_gms_fonts_certs
)

val fontName = GoogleFont("Rubik")

val rubik = FontFamily(
    Font(googleFont = fontName, fontProvider = provider),
    Font(googleFont = fontName, fontProvider = provider, weight = FontWeight.W500),
    Font(googleFont = fontName, fontProvider = provider, weight = FontWeight.W700)
)
val defautlTypography = Typography()
val PolyxTypography = Typography(
    h1 = defautlTypography.h1.copy(fontFamily = rubik, fontWeight = FontWeight.W700),
    h2 = defautlTypography.h2.copy(fontFamily = rubik, fontWeight = FontWeight.W700),
    h3 = defautlTypography.h3.copy(fontFamily = rubik, fontWeight = FontWeight.W700),
    h4 = defautlTypography.h4.copy(fontFamily = rubik, fontWeight = FontWeight.W700),
    h5 = defautlTypography.h5.copy(fontFamily = rubik, fontWeight = FontWeight.W700),
    h6 = defautlTypography.h6.copy(fontFamily = rubik, fontWeight = FontWeight.W500),
    subtitle1 = defautlTypography.subtitle1.copy(fontFamily = rubik, fontWeight = FontWeight.W500),
    subtitle2 = defautlTypography.subtitle2.copy(fontFamily = rubik, fontWeight = FontWeight.W500),
    body1 = defautlTypography.body1.copy(fontFamily = rubik, lineHeight = 28.sp),
    body2 = defautlTypography.body2.copy(fontFamily = rubik, lineHeight = 16.sp),
    button = defautlTypography.button.copy(fontFamily = rubik, fontWeight = FontWeight.W500),
    caption = defautlTypography.caption.copy(fontFamily = rubik),
    overline = defautlTypography.overline.copy(fontFamily = rubik)
)
