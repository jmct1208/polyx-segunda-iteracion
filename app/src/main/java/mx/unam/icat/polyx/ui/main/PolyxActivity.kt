package mx.unam.icat.polyx.ui.main

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import androidx.navigation.compose.rememberNavController
import androidx.navigation.toRoute
import dagger.hilt.android.AndroidEntryPoint
import mx.unam.icat.polyx.CourseRoute
import mx.unam.icat.polyx.FillInTheBlankRoute
import mx.unam.icat.polyx.GameRoute
import mx.unam.icat.polyx.GameTypeUnitsRoute
import mx.unam.icat.polyx.ImageRoute
import mx.unam.icat.polyx.QuizResultsRoute
import mx.unam.icat.polyx.QuizRoute
import mx.unam.icat.polyx.R
import mx.unam.icat.polyx.UnitRoute
import mx.unam.icat.polyx.model.gameTypes.GameTypeEnum
import mx.unam.icat.polyx.model.section.SectionEntity
import mx.unam.icat.polyx.ui.course.CourseScreen
import mx.unam.icat.polyx.ui.gameTypeUnits.GameTypeUnitsScreen
import mx.unam.icat.polyx.ui.image.ImageScreen
import mx.unam.icat.polyx.ui.quiz.QuizResultsScreen
import mx.unam.icat.polyx.ui.quiz.QuizScreen
import mx.unam.icat.polyx.ui.quiz.QuizViewModel
import mx.unam.icat.polyx.ui.theme.PolyxTheme
import mx.unam.icat.polyx.ui.unit.UnitScreen

@AndroidEntryPoint
class PolyxActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PolyxTheme {
                PolyxApp()
            }
        }
    }
}

@Composable
fun PolyxApp() {
    val controller = rememberNavController()
    PolyxNavHost(navController = controller)
}

@Composable
fun PolyxNavHost(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        startDestination = CourseRoute(stringResource(R.string.course_temas_selectos))
    ) {
        composable<CourseRoute> {
            CourseScreen(
                navigateToUnitScreen = { sectionEntity ->
                    if (sectionEntity.type == SectionEntity.Type.IMAGE_TITLE) {
                        navController.navigate(
                            UnitRoute(
                                unitId = sectionEntity.sectionId,
                                initialScrollSectionId = sectionEntity.sectionId
                            )
                        )
                    }
                },
                navigateToGameTypeUnitListScreen = { courseId, gameType ->
                    if (courseId != null) {
                        navController.navigate(
                            GameTypeUnitsRoute(
                                courseId = courseId,
                                gameTypeName = gameType.name.name
                            )
                        )
                    }
                }
            )
        }
        composable<UnitRoute> {
            UnitScreen(
                navigateUp = navController::navigateUp,
                navigateToImageDialog = {
                    navController.navigate(
                        ImageRoute(imageUrl = it)
                    )
                }
            )
        }
        composable<ImageRoute> {
            ImageScreen(
                resourcePath = it.toRoute<ImageRoute>().imageUrl,
                onClosePressed = navController::navigateUp
            )
        }

        composable<GameTypeUnitsRoute> {
            val route = it.toRoute<GameTypeUnitsRoute>()
            GameTypeUnitsScreen(
                navigateUp = navController::navigateUp,
                navigateToGame = { unit: SectionEntity ->
                    if (unit.type == SectionEntity.Type.IMAGE_TITLE) {
                        when (GameTypeEnum.valueOf(route.gameTypeName)) {
                            GameTypeEnum.QUIZ -> navController.navigate(
                                GameRoute(unitId = unit.sectionId)
                            )

                            GameTypeEnum.FILL_IN_THE_BLANK -> navController.navigate(
                                FillInTheBlankRoute(unitId = unit.sectionId)
                            )

                            GameTypeEnum.MATCH_COLUMNS -> {}
                        }
                    }
                }
            )
        }
        navigation<GameRoute>(startDestination = QuizRoute) {
            composable<QuizRoute> {
                QuizScreen(
                    navigateUp = navController::navigateUp,
                    navigateToResults = {
                        navController.navigate(QuizResultsRoute)
                    }
                )
            }
            composable<QuizResultsRoute> { backStackEntry ->
                val parentEntry = remember(backStackEntry) {
                    navController.getBackStackEntry(QuizRoute)
                }
                val parentViewModel = hiltViewModel<QuizViewModel>(parentEntry)
                QuizResultsScreen(
                    viewModel = parentViewModel,
                    navigateUp = navController::navigateUp,
                    onStudySubunitPressed = { unitId, closestParentSectionId ->
                        Log.d(PolyxActivity::class.simpleName, "Unit id: $unitId")
                        navController.navigate(
                            UnitRoute(
                                unitId = unitId,
                                initialScrollSectionId = closestParentSectionId
                            )
                        )
                    }
                )
            }
        }
    }
}