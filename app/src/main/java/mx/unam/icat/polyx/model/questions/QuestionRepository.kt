package mx.unam.icat.polyx.model.questions

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QuestionRepository @Inject constructor(private val quizQuestionDao: QuizQuestionDao) {
    suspend fun getQuestionsWithOptionsByUnitId(unitId: Long): List<QuizQuestionWithQuizOptions> {
        return quizQuestionDao.getQuestionsWithOptionsByUnitId(unitId)
    }
}