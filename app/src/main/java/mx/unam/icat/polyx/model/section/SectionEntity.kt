package mx.unam.icat.polyx.model.section

import androidx.room.*
@Entity(
    tableName = "section", foreignKeys = [ForeignKey(
        entity = SectionEntity::class,
        parentColumns = ["section_id"],
        childColumns = ["section_owner_id"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.SET_DEFAULT
    )]
)
data class SectionEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "section_id")
    val sectionId: Long = 0,
    @ColumnInfo(name = "child_order")
    val childOrder: Long,
    @ColumnInfo(name = "section_owner_id", index = true)
    val sectionOwnerId: Long?,
    @ColumnInfo(name = "title_1")
    val title1: String?,
    @ColumnInfo(name = "title_2")
    val title2: String?,
    @ColumnInfo(name = "text_content_1")
    val textContent1: String?,
    @ColumnInfo(name = "text_content_2")
    val textContent2: String?,
    @ColumnInfo(name = "resource_path_1")
    val resourcePath1: String?,
    @ColumnInfo(name = "resource_path_2")
    val resourcePath2: String?,
    @ColumnInfo(name = "section_type")
    val type: Type
) {
    enum class Type {
        TITLE,
        TEXT,
        IMAGE,
        TWO_IMAGES,
        TWO_SUBTITLES_TWO_IMAGES,
        TWO_SUBTITLES_TWO_TEXTS,
        TWO_SUBTITLES_TWO_TEXTS_TWO_IMAGES,
        TWO_SUBTITLES_TWO_IMAGES_TWO_TEXTS,
        IMAGE_TITLE
    }
}
