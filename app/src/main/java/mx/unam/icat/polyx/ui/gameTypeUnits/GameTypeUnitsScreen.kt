package mx.unam.icat.polyx.ui.gameTypeUnits

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import mx.unam.icat.polyx.R
import mx.unam.icat.polyx.home.presentation.VerticalUnitCard
import mx.unam.icat.polyx.model.section.SectionEntity
import mx.unam.icat.polyx.ui.PolyxTopBar
import mx.unam.icat.polyx.ui.theme.PolyxTheme


@Composable
fun GameTypeUnitsScreen(
    viewModel: GameTypeUnitsViewModel = hiltViewModel(),
    navigateUp: () -> Unit,
    navigateToGame: (SectionEntity) -> Unit
) {
    val unitList by viewModel.units.collectAsState()
    Log.d("GameTypeUnitsScreen", "$unitList")
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            PolyxTopBar(
                title = viewModel.route.gameTypeName,
                navigationIcon = R.drawable.ic_baseline_arrow_back_24,
                navigateUp = navigateUp
            )
        }
    ) { innerPadding: PaddingValues -> //this value is zero
        ToGameTypeUnitList(
            unitList = unitList,
            modifier = Modifier.padding(innerPadding),
            navigateToGame = navigateToGame
        )
    }
}

@Composable
fun ToGameTypeUnitList(
    unitList: List<SectionEntity>,
    modifier: Modifier = Modifier,
    navigateToGame: (SectionEntity) -> Unit
) {
    LazyVerticalGrid(
        columns = GridCells.Adaptive(minSize = 128.dp),
        verticalArrangement = Arrangement.spacedBy(4.dp),
        horizontalArrangement = Arrangement.spacedBy(4.dp),
        modifier = modifier,
        contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp)
    ) {
        Log.d(javaClass.simpleName, "Unit list: $unitList")
        items(unitList) {
            VerticalUnitCard(sectionEntity = it, onUnitClicked = navigateToGame)
        }
    }
}

@Preview
@Composable
fun ToGameTypeUnitListPreview() {
    PolyxTheme {
        GameTypeUnitsScreen(
            navigateUp = { },
            navigateToGame = { _ -> }
        )
    }
}