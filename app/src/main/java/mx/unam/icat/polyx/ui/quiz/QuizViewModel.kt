package mx.unam.icat.polyx.ui.quiz

import android.util.Log
import androidx.lifecycle.*
import androidx.navigation.toRoute
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import mx.unam.icat.polyx.GameRoute
import mx.unam.icat.polyx.model.questions.QuestionRepository
import mx.unam.icat.polyx.model.questions.QuizQuestionWithQuizOptions
import mx.unam.icat.polyx.model.section.SectionRepository
import mx.unam.icat.polyx.ui.Result
import mx.unam.icat.polyx.utils.PausableCountDownTimer
import javax.inject.Inject

@HiltViewModel
class QuizViewModel @Inject constructor(
    questionRepository: QuestionRepository,
    private val sectionRepository: SectionRepository,
    state: SavedStateHandle
) : ViewModel() {
    private val route = state.toRoute<GameRoute>()
    private val questions =
        MutableStateFlow<Result<List<QuizQuestionWithQuizOptions>>>(Result.Loading)
    private val pausableTimer =
        object : PausableCountDownTimer(INITIAL_TIMER_VALUE, COUNTDOWN_INTERVAL) {
            override fun onTick(tickCounter: Int) {
                _quizUiState.update {
                    when (it) {
                        is QuizUiState.Success -> it.copy(timeRemaining = tickCounter)
                        else -> it
                    }
                }
            }

            override fun onFinish() {
                viewModelScope.launch {
                    val quizState = quizUiState.value
                    if (quizState is QuizUiState.Success) {
                        val quizAnswer = getAnswerType(
                            quizState.quizQuestionWithQuizOptions,
                            quizState.selectedOption
                        )
                        _answerUiState.update {
                            buildList {
                                addAll(it)
                                add(quizAnswer)
                            }
                        }
                        _quizUiState.update {
                            (it as QuizUiState.Success).copy(
                                quizAnswer = quizAnswer,
                                selectedOption = it.selectedOption
                            )
                        }
                    }
                }
            }
        }
    private val _answerUiState = MutableStateFlow<List<QuizAnswer>>(listOf())
    private val _quizUiState = MutableStateFlow<QuizUiState>(QuizUiState.Loading)
    val quizUiState = _quizUiState.asStateFlow()
    val answerUiState = _answerUiState.asStateFlow()

    init {
        viewModelScope.launch {
            val questionsResult =
                Result.Success(questionRepository.getQuestionsWithOptionsByUnitId(route.unitId))
            questions.value = questionsResult
            if (questionsResult.data.isEmpty()) {
                _quizUiState.value = QuizUiState.QuizOver
            } else {
                _quizUiState.value = QuizUiState.Success(
                    quizQuestionWithQuizOptions = questionsResult.data[0],
                    selectedOption = null,
                    timeRemaining = INITIAL_TIMER_VALUE,
                    quizAnswer = null,
                    timerState = pausableTimer.currentState,
                    currentQuestionIndex = 0,
                    numberOfQuestions = questionsResult.data.size
                )
            }
        }
    }


    private suspend fun getAnswerType(
        quizQuestionWithQuizOptions: QuizQuestionWithQuizOptions,
        selectedOption: Int?
    ): QuizAnswer {
        val correctOptionIndex = quizQuestionWithQuizOptions.options.indexOfFirst { option ->
            option.isAnswer
        }
        val question = quizQuestionWithQuizOptions.questionEntity.text
        val correctOption = quizQuestionWithQuizOptions.options[correctOptionIndex].text!!
        val sectionOwnerId = quizQuestionWithQuizOptions.questionEntity.sectionOwnerId!!

        val titleSection = sectionRepository.getClosestParentTitleSection(
            sectionOwnerId
        )
        val closestParentSectionId =
            if (titleSection.isEmpty()) route.unitId else titleSection[0].sectionId
        val quizAnswer: QuizAnswer = if (selectedOption == null) {
            NoQuizAnswer(
                question = question,
                correctOption = correctOption,
                sectionOwnerId = sectionOwnerId,
                unitId = route.unitId,
                closestParentSectionId = closestParentSectionId
            )
        } else {
            if (selectedOption == correctOptionIndex)
                CorrectQuizAnswer(
                    question = question,
                    correctOption = correctOption
                )
            else
                IncorrectQuizAnswer(
                    question = question,
                    correctOption = correctOption,
                    selectedOption = quizQuestionWithQuizOptions.options[selectedOption].text!!,
                    sectionOwnerId = sectionOwnerId,
                    unitId = route.unitId,
                    closestParentSectionId = closestParentSectionId,
                )
        }
        return quizAnswer
    }

    fun startTimer() {
        pausableTimer.start()
        _quizUiState.update {
            when (it) {
                is QuizUiState.Success -> it.copy(timerState = pausableTimer.currentState)
                else -> it
            }
        }
    }

    fun answerQuestion() {
        pausableTimer.cancel()
        viewModelScope.launch {
            val state = quizUiState.value
            if (state is QuizUiState.Success) {
                val quizAnswer =
                    getAnswerType(state.quizQuestionWithQuizOptions, state.selectedOption)
                _answerUiState.update {
                    buildList {
                        addAll(it)
                        add(quizAnswer)
                    }
                }
                if (state.quizAnswer == null) {
                    _quizUiState.update {
                        (it as QuizUiState.Success).copy(quizAnswer = quizAnswer)
                    }
                }
            }
        }
    }

    fun pauseTimer() {
        pausableTimer.pause()
        _quizUiState.update {
            when (it) {
                is QuizUiState.Success -> it.copy(timerState = pausableTimer.currentState)
                else -> it
            }
        }
    }

    fun changeToNextQuestion() {
        pausableTimer.reset()
        try {
            _quizUiState.update {
                if (it is QuizUiState.Success && it.quizAnswer != null) {
                    it.copy(
                        quizQuestionWithQuizOptions = ((questions.value) as Result.Success).data[it.currentQuestionIndex + 1],
                        selectedOption = null,
                        timeRemaining = INITIAL_TIMER_VALUE,
                        quizAnswer = null,
                        timerState = pausableTimer.currentState,
                        currentQuestionIndex = it.currentQuestionIndex + 1
                    )
                }
                else it
            }
        } catch (e: IndexOutOfBoundsException) {
            _quizUiState.update {
                QuizUiState.QuizOver
            }
        }
    }

    fun optionClicked(option: Int?) {
        _quizUiState.update {
            if (it is QuizUiState.Success && it.quizAnswer == null)
                it.copy(selectedOption = option)
            else it
        }
    }

    companion object {
        const val INITIAL_TIMER_VALUE: Int = 15
        const val COUNTDOWN_INTERVAL: Int = 1000
    }

    override fun onCleared() {
        super.onCleared()
        Log.d(QuizViewModel::class.simpleName, "Cleared")
    }
}

sealed interface QuizAnswer
data class NoQuizAnswer(
    val question: String?,
    val correctOption: String,
    val sectionOwnerId: Long,
    val unitId: Long,
    val closestParentSectionId: Long
) : QuizAnswer

data class CorrectQuizAnswer(val question: String?, val correctOption: String) : QuizAnswer
data class IncorrectQuizAnswer(
    val question: String?,
    val correctOption: String,
    val selectedOption: String,
    val sectionOwnerId: Long,
    val unitId: Long,
    val closestParentSectionId: Long
) : QuizAnswer


sealed interface QuizUiState {
    data object Loading : QuizUiState
    data object QuizOver : QuizUiState
    data class Success(
        val quizQuestionWithQuizOptions: QuizQuestionWithQuizOptions,
        val selectedOption: Int?,
        val timeRemaining: Int,
        val quizAnswer: QuizAnswer?,
        val timerState: PausableCountDownTimer.TimerState,
        val currentQuestionIndex: Int,
        val numberOfQuestions: Int
    ) : QuizUiState
}

/*data class InitialState(
    val quizQuestionWithQuizOptions: QuizQuestionWithQuizOptions
) : QuizUiState

data class TimeRunning(
    val quizQuestionWithQuizOptions: QuizQuestionWithQuizOptions,
    val selectedOption: Int?,
    val timeRemaining: Int,
) : QuizUiState

data class QuestionAnswered(
    val quizQuestionWithQuizOptions: QuizQuestionWithQuizOptions,
    val selectedOption: Int?,
    val timeRemaining: Int,
    val quizAnswer: QuizAnswer
) : QuizUiState*/



