package mx.unam.icat.polyx.model.quizOptions

import androidx.room.*
import mx.unam.icat.polyx.model.questions.QuestionEntity

@Entity(
    tableName = "quiz_question_quiz_option",
    foreignKeys = [ForeignKey(
        entity = QuestionEntity::class,
        parentColumns = ["quiz_question_id"],
        childColumns = ["quiz_question_owner_id"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    ), ForeignKey(
        entity = QuizOption::class,
        parentColumns = ["quiz_option_id"],
        childColumns = ["quiz_option_owner_id"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )],
    primaryKeys = ["quiz_question_owner_id", "quiz_option_owner_id"]
)
data class QuizQuestionQuizOption(
    @ColumnInfo(name = "quiz_question_owner_id", index = true) val quizQuestionOwnerId: Long,
    @ColumnInfo(name = "quiz_option_owner_id", index = true) val quizOptionOwnerId: Long
)