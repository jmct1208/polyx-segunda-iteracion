package mx.unam.icat.polyx.model.section

import androidx.room.*

@Dao
interface SectionDao {
    @Transaction
    @Query(
        "WITH RECURSIVE under_section AS (" +
                "SELECT s0.*, 0 as depth FROM section s0  WHERE section_id = :sectionId " +
                "UNION ALL " +
                "SELECT s1.*, u_s.depth + 1 " +
                "FROM section AS s1 JOIN under_section AS u_s  ON s1.section_owner_id=u_s.section_id " +
                "ORDER BY 11 DESC, 1) " +
                "SELECT * FROM under_section u WHERE :includeRootSection OR u.depth != 0;"
    )
    suspend fun getSectionSubTree(
        sectionId: Long,
        includeRootSection: Boolean
    ): Map<SectionEntity, @MapColumn(columnName = "depth") Int>

    @Transaction
    @Query(
        "WITH RECURSIVE under_section AS (" +
                "SELECT s0.*, 0 as depth FROM section s0  WHERE section_id = :sectionId " +
                "UNION ALL " +
                "SELECT s1.*, u_s.depth + 1 " +
                "FROM section AS s1 JOIN under_section AS u_s  ON s1.section_owner_id=u_s.section_id " +
                "ORDER BY 11 DESC, 1) " +
                "SELECT * FROM under_section u WHERE (:includeRootSection OR u.depth != 0) " +
                "AND u.section_type IN ('TITLE', 'IMAGE_TITLE');"
    )
    suspend fun getSectionSubTreeTitleOrTextImageTypes(
        sectionId: Long,
        includeRootSection: Boolean
    ): Map<SectionEntity, @MapColumn(columnName = "depth") Int>

    @Transaction
    @Query(
        "WITH RECURSIVE under_section AS (" +
                "SELECT s0.*, 0 as depth FROM section s0  WHERE section_id = :sectionId " +
                "UNION ALL " +
                "SELECT s1.*, u_s.depth + 1 " +
                "FROM section AS s1 JOIN under_section u_s ON s1.section_owner_id=u_s.section_id " +
                "WHERE u_s.depth  < :depth " +
                "ORDER BY 11 DESC, 1)" +
                "SELECT * FROM under_section u WHERE :includeRootSection OR u.depth != 0 ;"
    )
    suspend fun getSectionSubtreeUpToDepth(
        sectionId: Long,
        depth: Int,
        includeRootSection: Boolean
    ): Map<SectionEntity, @MapColumn(columnName = "depth") Int>

    @Transaction
    @Query(
        "WITH RECURSIVE above_section AS (" +
                "SELECT  s0.* " +
                "FROM  section s0 JOIN quiz_question q on q.section_owner_id = s0.section_id " +
                "UNION ALL " +
                "SELECT s1.*" +
                "FROM above_section\n" +
                "JOIN section s1 " +
                "ON s1.section_id = above_section.section_owner_id) " +
                "SELECT DISTINCT * " +
                "FROM above_section " +
                "WHERE section_owner_id = :courseId"
    )
    suspend fun getUnitsThatHaveQuestions(courseId: Long): List<SectionEntity>

    @Transaction
    @Query(
        "WITH RECURSIVE above_section AS (" +
                "SELECT  s0.* " +
                "FROM  section s0 JOIN fill_in_the_blank_sentence f on f.section_owner_id = s0.section_id " +
                "UNION ALL " +
                "SELECT s1.*" +
                "FROM above_section\n" +
                "JOIN section s1 " +
                "ON s1.section_id = above_section.section_owner_id) " +
                "SELECT DISTINCT * " +
                "FROM above_section " +
                "WHERE section_owner_id = :courseId"
    )
    suspend fun getUnitsHavingSentences(courseId: Long): List<SectionEntity>


    @Transaction
    @Query(
        "WITH RECURSIVE above_section AS (" +
                "SELECT s0.*, 0 as depth FROM section s0  WHERE section_id = :sectionId " +
                "UNION ALL " +
                "SELECT s1.*, a_s.depth + 1 FROM section AS s1 " +
                "JOIN above_section a_s " +
                "ON a_s.section_owner_id=s1.section_id ORDER BY 11 DESC) " +
                "SELECT u.section_id, u.section_owner_id, u.child_order, u.title_1, u.title_2, " +
                "u.text_content_1, u.text_content_2, u.resource_path_1, u.resource_path_2, " +
                "u.section_type FROM above_section u " +
                "WHERE u.depth = (SELECT MIN(u1.depth) FROM above_section u1 WHERE u1.section_type = 'TITLE')"
    )
    suspend fun getClosesParentTitleSection(sectionId: Long): List<SectionEntity>

    @Insert
    suspend fun insertSection(sectionEntity: SectionEntity): Long

    @Query("SELECT section_id FROM section WHERE title_1 = :title")
    suspend fun getCourseIdByName(title: String): Long?

}

