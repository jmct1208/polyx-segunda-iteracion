package mx.unam.icat.polyx.model.section

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SectionRepository @Inject constructor(private val sectionDao: SectionDao) {
    suspend fun getSectionSubtree(
        sectionId: Long,
        includeRootSection: Boolean
    ): Map<SectionEntity, Int> =
        sectionDao.getSectionSubTree(
            sectionId,
            includeRootSection
        )


    private suspend fun getSectionSubtreeUpToDepth(
        sectionId: Long,
        depth: Int,
        includeRootSection: Boolean
    ): Map<SectionEntity, Int> =
        sectionDao.getSectionSubtreeUpToDepth(
            sectionId,
            depth,
            includeRootSection
        )


    suspend fun getUnitsThatHaveQuestions(courseId: Long): List<SectionEntity> =
        sectionDao.getUnitsThatHaveQuestions(courseId).map {
            if (it.type != SectionEntity.Type.IMAGE_TITLE) throw IllegalStateException()
            else it

        }

    suspend fun getUnitsHavingSentences(courseId: Long): List<SectionEntity> =
        sectionDao.getUnitsHavingSentences(courseId).map {
            if (it.type != SectionEntity.Type.IMAGE_TITLE) throw IllegalStateException()
            else it

        }

    suspend fun getUnitsOfCourse(courseId: Long): List<SectionEntity> {
        return getSectionSubtreeUpToDepth(
            courseId,
            1,
            false
        ).keys.toList()
    }

    suspend fun getSectionSubTreeTitleOrTitleImageTypes(
        sectionId: Long,
        includeRootSection: Boolean
    ): Map<SectionEntity, Int> =
        sectionDao.getSectionSubTreeTitleOrTextImageTypes(
            sectionId,
            includeRootSection
        )

    suspend fun getCourseIdByTitle(title: String): Long? =
        sectionDao.getCourseIdByName(title)

    suspend fun getClosestParentTitleSection(sectionId: Long) =
        sectionDao.getClosesParentTitleSection(sectionId)

}