package mx.unam.icat.polyx.model.fillintheblankcorrectoption

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import mx.unam.icat.polyx.model.fillintheblanksentence.FillInTheBlankSentence

@Entity(
    tableName = "fill_in_the_blank_correct_option",
    foreignKeys = [ForeignKey(
        entity = FillInTheBlankSentence::class,
        parentColumns = ["fill_in_the_blank_sentence_id"],
        childColumns = ["fill_in_the_blank_sentence_owner_id"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.SET_NULL
    )])
data class FillInTheBlankCorrectOption(
    @ColumnInfo(name = "fill_in_the_blank_sentence_owner_id", index = true)
    val sentenceOwnerId: Long?,
    val position: Int,
    val length: Int,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "fill_in_the_blank_correct_option_id")
    val fillInTheBlankCorrectOptionId: Long = 0,
)