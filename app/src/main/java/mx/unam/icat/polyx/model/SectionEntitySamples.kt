package mx.unam.icat.polyx.model

import mx.unam.icat.polyx.model.section.SectionEntity

val unit1 = SectionEntity(
    sectionId = 32,
    sectionOwnerId = 31,
    childOrder = 1,
    title1 = "Conceptos básicos de polímeros",
    textContent1 = null,
    resourcePath1 = "unit_icons/escructura_polimeros.png",
    type = SectionEntity.Type.IMAGE_TITLE,
    title2 = null,
    resourcePath2 = null,
    textContent2 = null
)

val unit2 = SectionEntity(
    sectionId = 33,
    sectionOwnerId = 31,
    childOrder = 2,
    title1 = "Propiedades mecánicas",
    textContent1 = null,
    resourcePath1 = "unit_icons/escructura_polimeros.png",
    type = SectionEntity.Type.IMAGE_TITLE,
    title2 = null,
    resourcePath2 = null,
    textContent2 = null
)

val unit3 = SectionEntity(
    sectionId = 34,
    sectionOwnerId = 31,
    childOrder = 3,
    title1 = "Análisis térmico",
    textContent1 = null,
    resourcePath1 = "unit_icons/escructura_polimeros.png",
    type = SectionEntity.Type.IMAGE_TITLE,
    title2 = null,
    resourcePath2 = null,
    textContent2 = null
)

val unit4 = SectionEntity(
    sectionId = 35,
    sectionOwnerId = 31,
    childOrder = 4,
    title1 = "Reología",
    textContent1 = null,
    resourcePath1 = "unit_icons/escructura_polimeros.png",
    type = SectionEntity.Type.IMAGE_TITLE,
    title2 = null,
    resourcePath2 = null,
    textContent2 = null
)

val unit5 = SectionEntity(
    sectionId = 36,
    sectionOwnerId = 31,
    childOrder = 5,
    title1 = "Microscopía",
    textContent1 = null,
    resourcePath1 = "unit_icons/escructura_polimeros.png",
    type = SectionEntity.Type.IMAGE_TITLE,
    title2 = null,
    resourcePath2 = null,
    textContent2 = null
)

val unit6 = SectionEntity(
    sectionId = 37,
    sectionOwnerId = 31,
    childOrder = 6,
    title1 = "Determinación de pesos moleculares",
    textContent1 = null,
    resourcePath1 = "unit_icons/escructura_polimeros.png",
    type = SectionEntity.Type.IMAGE_TITLE,
    title2 = null,
    resourcePath2 = null,
    textContent2 = null
)

val unit7 = SectionEntity(
    sectionId = 38,
    sectionOwnerId = 31,
    childOrder = 7,
    title1 = "Métodos espectroscópicos",
    textContent1 = null,
    resourcePath1 = "unit_icons/escructura_polimeros.png",
    type = SectionEntity.Type.IMAGE_TITLE,
    title2 = null,
    resourcePath2 = null,
    textContent2 = null
)

val unitSampleList = listOf(unit1, unit2, unit3, unit4, unit5, unit6, unit7)