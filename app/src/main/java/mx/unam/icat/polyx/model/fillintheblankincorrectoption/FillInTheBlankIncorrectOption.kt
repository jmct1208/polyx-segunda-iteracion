package mx.unam.icat.polyx.model.fillintheblankincorrectoption

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "fill_in_the_blank_incorrect_option")
data class FillInTheBlankIncorrectOption(
    val text: String,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "fill_in_the_blank_incorrect_option_id")
    val fillInTheBlankIncorrectOptionId: Long = 0
)