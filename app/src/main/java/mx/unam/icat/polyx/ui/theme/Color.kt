package mx.unam.icat.polyx.ui.theme
import androidx.compose.ui.graphics.Color

val Black = Color(0xFF000000)
val White = Color(0xFFFFFFFF)
val TransparentBlack = Color(0xCD000000)
val LightBlue500 = Color(0xFF03A9F4)
val LightBlue700 = Color(0xFF0288D1)
val LightBlue100 = Color(0xFFB3E5FC)
val Orange500 = Color(0xFFFF9800)
val Orange700 = Color(0xFFF57C00)
val Orange100 = Color(0xFFFFE0B2)
val TransparentRed = Color(0x0AF44336)
val TransparentGreen = Color(0x0A4CAF50)
val TransparentYellow = Color(0x0AFFEB3B)
val md_theme_light_outline = Color(0xFF7B757F)
val md_theme_dark_outline = Color(0xFF958E99)
val incorrectAnswerRed = Color(0xFFF44336)
val correctAnswerGreen = Color(0xFF4CAF50)
val noAnswerYellow = Color(0xFFFFEB3B)

