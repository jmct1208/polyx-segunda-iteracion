package mx.unam.icat.polyx.model.quizOptions

import androidx.room.Dao
import androidx.room.Insert

@Dao
interface QuizOptionDao {
    @Insert
    suspend fun insertQuizOption(quizOption: QuizOption): Long
}