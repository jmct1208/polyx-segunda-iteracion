package mx.unam.icat.polyx.home.presentation

import android.content.res.Configuration
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import mx.unam.icat.polyx.R
import mx.unam.icat.polyx.model.gameTypes.GameType
import mx.unam.icat.polyx.model.gameTypes.GameTypeEnum
import mx.unam.icat.polyx.model.section.SectionEntity
import mx.unam.icat.polyx.ui.AssetImage
import mx.unam.icat.polyx.ui.theme.PolyxTheme

@Composable
fun UnitCard(
    sectionEntity: SectionEntity,
    onUnitClicked: (SectionEntity) -> Unit
) {
    if (sectionEntity.resourcePath1 != null && sectionEntity.title1 != null) {
        HorizontalThumbnailAndTextCard(
            sectionEntity.resourcePath1,
            sectionEntity.title1,
            Modifier.clickable { onUnitClicked(sectionEntity) })
    }
}

@Composable
fun GameTypeCard(
    gameType: GameType,
    onGameTypeClicked: (GameType) -> Unit
) {
    val gameTypeString = when (gameType.name) {
        GameTypeEnum.QUIZ -> stringResource(id = R.string.quiz_gametype_name)
        GameTypeEnum.FILL_IN_THE_BLANK -> stringResource(id = R.string.fill_in_the_blank)
        GameTypeEnum.MATCH_COLUMNS -> stringResource(id = R.string.match_columns_gametype_name)
    }
    HorizontalThumbnailAndTextCard(
        gameType.resourcePath,
        gameTypeString,
        Modifier.clickable { onGameTypeClicked(gameType) }
    )

}

@Composable
private fun HorizontalThumbnailAndTextCard(
    resourcePath: String,
    text: String,
    modifier: Modifier = Modifier
) {
    Surface {
        Row(modifier = modifier) {
            AssetImage(
                resourcePath = resourcePath,
                modifier = Modifier
                    .padding(16.dp)
                    .size(40.dp, 40.dp)
                    .clip(MaterialTheme.shapes.small)

            )
            Text(
                text = text,
                modifier = Modifier
                    .weight(1f)
                    .padding(top = 10.dp, bottom = 10.dp, end = 16.dp),
                maxLines = 3,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.subtitle1
            )
        }
    }
}

@Composable
fun HorizontalThumbnailAndTextCardDivider() {
    Divider(modifier = Modifier.padding(horizontal = 16.dp))
}

@Composable
private fun VerticalThumbnailAndTextCard(
    resourcePath: String,
    text: String,
    modifier: Modifier = Modifier
) {
    Surface {
        Column(modifier = modifier) {
            AssetImage(
                resourcePath = resourcePath,
                modifier = Modifier
                    .padding(16.dp)
                    .size(40.dp, 40.dp)
                    .clip(MaterialTheme.shapes.small)

            )
            Text(
                text = text,
                modifier = Modifier.padding(8.dp),
                maxLines = 3,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.subtitle1
            )
        }
    }
}

@Composable
fun VerticalUnitCard(sectionEntity: SectionEntity, onUnitClicked: (SectionEntity) -> Unit) {
    if (sectionEntity.resourcePath1 != null && sectionEntity.title1 != null) {
        VerticalThumbnailAndTextCard(
            sectionEntity.resourcePath1,
            sectionEntity.title1,
            Modifier
                .clickable { onUnitClicked(sectionEntity) }
                .border(0.5.dp, MaterialTheme.colors.onSurface.copy(alpha = 0.12f))
                .size(width = 170.dp, height = 250.dp)
                .aspectRatio(170f / 250f)
        )
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun VerticalUnitCardPreview() {
    val sampleSectionEntity = SectionEntity(
        sectionId = 32,
        childOrder = 1,
        sectionOwnerId = 31,
        title1 = "Conceptos básicos de polímeros",
        title2 = null,
        textContent1 = null,
        textContent2 = null,
        resourcePath1 = "unit_icons/escructura_polimeros.png",
        resourcePath2 = null,
        type = SectionEntity.Type.IMAGE_TITLE
    )
    PolyxTheme {
        VerticalUnitCard(sampleSectionEntity) { }
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun GameTypeCardPreview() {
    val gameTypeSample = GameType(
        GameTypeEnum.QUIZ,
        "unit_icons/escructura_polimeros.png"
    )
    PolyxTheme {
        GameTypeCard(gameTypeSample) {}
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun VerticalListUnitItemPreview() {
    val sampleSectionEntity = SectionEntity(
        sectionId = 32,
        childOrder = 1,
        sectionOwnerId = 31,
        title1 = "Conceptos básicos de polímeros",
        title2 = null,
        textContent1 = null,
        textContent2 = null,
        resourcePath1 = "unit_icons/escructura_polimeros.png",
        resourcePath2 = null,
        type = SectionEntity.Type.IMAGE_TITLE
    )
    PolyxTheme {
        UnitCard(sampleSectionEntity) {}
    }
}