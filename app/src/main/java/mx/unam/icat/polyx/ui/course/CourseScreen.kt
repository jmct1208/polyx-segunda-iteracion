package mx.unam.icat.polyx.ui.course

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.text.toUpperCase
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import kotlinx.coroutines.launch
import mx.unam.icat.polyx.R
import mx.unam.icat.polyx.model.gameTypes.GameType
import mx.unam.icat.polyx.model.section.SectionEntity

@Composable
fun CourseScreen(
    courseViewModel: CourseViewModel = hiltViewModel(),
    navigateToUnitScreen: (SectionEntity) -> Unit,
    navigateToGameTypeUnitListScreen: (Long?, GameType) -> Unit
) {
    val gameTypesUiState by courseViewModel.gameTypes.collectAsState()
    val unitListUiState by courseViewModel.units.collectAsState()

    Scaffold { innerPadding: PaddingValues ->
        CourseScreenPager(
            gameTypesUiState = gameTypesUiState,
            unitListUiState = unitListUiState,
            navigateToUnitScreen = navigateToUnitScreen,
            navigateToGameTypeUnitListScreen = navigateToGameTypeUnitListScreen,
            modifier = Modifier.padding(innerPadding)
        )
    }
}

@Composable
fun CourseScreenPager(
    gameTypesUiState: GameTypesUiState,
    unitListUiState: UnitListUiState,
    navigateToUnitScreen: (SectionEntity) -> Unit,
    navigateToGameTypeUnitListScreen: (Long, GameType) -> Unit,
    modifier: Modifier = Modifier
) {
    val pagerState = rememberPagerState(pageCount = { 2 })
    val titles = listOf(
        stringResource(R.string.units).toUpperCase(Locale("es-MX")),
        stringResource(R.string.game_category).toUpperCase(Locale("es-MX"))
    )
    val animationScope = rememberCoroutineScope()
    Column(modifier = modifier) {
        TabRow(
            selectedTabIndex = pagerState.currentPage,
            backgroundColor = MaterialTheme.colors.primary,
            tabs = {
                titles.forEachIndexed { index, title ->
                    Tab(
                        selected = pagerState.currentPage == index,
                        onClick = {
                            animationScope.launch { pagerState.animateScrollToPage(index) }
                        },
                        text = { Text(text = title) }
                    )
                }
            }
        )
        HorizontalPager(state = pagerState) { page ->
            if (page == 0) {
                when (unitListUiState) {
                    UnitListUiState.Error -> {}
                    UnitListUiState.Loading -> {}
                    is UnitListUiState.Success -> UnitList(
                        unitList = unitListUiState.units,
                        navigateToUnit = navigateToUnitScreen
                    )
                }


            }
            if (page == 1) {
                when (gameTypesUiState) {
                    GameTypesUiState.Loading -> {}
                    is GameTypesUiState.Success -> GameTypeList(
                        gameTypeList = gameTypesUiState.gameTypes,
                        navigateToGameTypeUnitsScreenByCourseId = {
                            navigateToGameTypeUnitListScreen(gameTypesUiState.courseId, it)
                        }
                    )
                    GameTypesUiState.Error -> {}
                }
            }
        }
    }
}

@Preview
@Composable
fun TabExample() {
    val pagerState = rememberPagerState { 10 }
    Column(modifier = Modifier.fillMaxSize()) {
        HorizontalPager(
            modifier = Modifier.weight(0.9f),
            state = pagerState
        ) { page ->
            Box(
                modifier = Modifier
                    .padding(10.dp)
                    .background(Color.Blue)
                    .fillMaxWidth()
                    .aspectRatio(1f), contentAlignment = Alignment.Center
            ) { Text(text = page.toString(), fontSize = 32.sp) }
        }
        Column(
            modifier = Modifier
                .weight(0.1f)
                .fillMaxWidth()
        ) {
            Text(text = "Current Page: ${pagerState.currentPage}")
            Text(
                text =
                "Target Page: ${pagerState.targetPage}"
            )
            Text(
                text =
                "Settled Page Offset: ${pagerState.settledPage}"
            )
        }
    }
}