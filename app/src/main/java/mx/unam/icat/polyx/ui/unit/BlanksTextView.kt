package mx.unam.icat.polyx.ui.unit

import android.content.Context
import android.graphics.*
import android.os.Build
import android.util.AttributeSet
import android.util.Log
import android.view.WindowInsets
import android.view.WindowManager
import com.google.android.material.textview.MaterialTextView

class BlanksTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MaterialTextView(context, attrs, defStyleAttr) {
    private var blankIndicesList: List<Pair<Int, Int>>? = null
        set(value) {
            if (value == null) return
            textViewRects = List(value.size) { Rect() }
            field = value
            invalidate()
            requestLayout()
        }
    private var textViewRects: List<Rect>? = null
    private val paint = Paint().apply {
        style = Paint.Style.FILL
        color = Color.GRAY
    }
    private var screenSize = Point()

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        //Log.d(TAG, "----------------------------------------------------------")
        blankIndicesList?.forEachIndexed { index, pair ->
            //Log.d(TAG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
            val startOffsetOfBlankText = pair.first
            val endOffsetOfBlankText = pair.second + 1

            /*Log.d(TAG, "Start offset of blank text: $startOffsetOfBlankText")
            Log.d(TAG, "End offset of blank text: $endOffsetOfBlankText")*/

            var startXCoordinatesOfBlank = layout.getPrimaryHorizontal(startOffsetOfBlankText)
            var endXCoordinatesOfBlank = layout.getPrimaryHorizontal(endOffsetOfBlankText)

            /*Log.d(TAG, "Start X coordinates of blank text: $startXCoordinatesOfBlank")
            Log.d(TAG, "End X coordinates of blank text: $endXCoordinatesOfBlank")*/

            val currentLineStartOffset = layout.getLineForOffset(startOffsetOfBlankText)
            val currentLineEndOffset = layout.getLineForOffset(endOffsetOfBlankText)

            /*Log.d(TAG, "Current line start offset: $currentLineStartOffset")
            Log.d(TAG, "Current line end offset: $currentLineEndOffset")*/

            val blankIsInMultiline = currentLineEndOffset != currentLineStartOffset

            //Log.d(TAG, "Is multiline: $blankIsInMultiline")

            layout.getLineBounds(currentLineStartOffset, textViewRects!![index])

            //Log.d(TAG, "Rectangle after calling getLineBounds with currentLine start offset: ${textViewRects!![index]}")

            val textViewLocation = intArrayOf(0, 0)
            getLocationOnScreen(textViewLocation)
            //Log.d(TAG, "Location on screen of text view: (${textViewLocation[0]}, ${textViewLocation[1]})")

            val textViewTopAndBottomOffset =
                /*textViewLocation[1]*/ + scrollY + compoundPaddingTop
            /*Log.d(TAG, "Scroll y: $scrollY")
            Log.d(TAG, "Compound padding top: $compoundPaddingTop")
            Log.d(TAG, "Top and Bottom offset: $textViewTopAndBottomOffset")*/

            textViewRects!![index].top += textViewTopAndBottomOffset
            textViewRects!![index].bottom += textViewTopAndBottomOffset

            if (blankIsInMultiline) {
                val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                val screenHeight = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    val metrics = wm.currentWindowMetrics
                    val windowInsets = metrics.windowInsets
                    val insets = windowInsets.getInsetsIgnoringVisibility(
                        WindowInsets.Type.navigationBars()
                                or WindowInsets.Type.displayCutout()
                    )
                    val insetsHeight = insets.top + insets.bottom
                    val bounds = metrics.bounds
                    bounds.height() - insetsHeight
                } else {
                    val display = wm.defaultDisplay
                    display.getSize(screenSize)
                    screenSize.y
                }

                val dyTop = textViewRects!![index].top
                val dyBottom = screenHeight - textViewRects!![index].bottom
                val onTop = dyTop > dyBottom

                if (onTop)
                    endXCoordinatesOfBlank = layout.getLineRight(currentLineStartOffset)
                else {
                    layout.getLineBounds(currentLineEndOffset, textViewRects!![index])
                    textViewRects!![index].top += textViewTopAndBottomOffset
                    textViewRects!![index].bottom += textViewTopAndBottomOffset
                    startXCoordinatesOfBlank = layout.getLineLeft(currentLineEndOffset)
                }
            }

            textViewRects!![index].left +=
                (/*textViewLocation[0]*/ +
                        startXCoordinatesOfBlank +
                        compoundPaddingLeft +
                        scrollX).toInt()
            /*Log.d(TAG,"Scroll x: $scrollX")
            Log.d(TAG, "Left compound padding: $compoundPaddingTop")
            Log.d(TAG, "X translation: ${textViewLocation[0] + startXCoordinatesOfBlank + compoundPaddingLeft + scrollX}")*/

            textViewRects!![index].right =
                (textViewRects!![index].left +
                        endXCoordinatesOfBlank -
                        startXCoordinatesOfBlank).toInt()

            Log.d(TAG, "Canvas object $canvas")
            canvas!!.drawRect(textViewRects!![index], paint)
        }
        Log.d(TAG, "Rectangle list: $textViewRects")
    }

    companion object {
        val TAG = BlanksTextView::class.simpleName
    }
}