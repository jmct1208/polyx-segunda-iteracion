package mx.unam.icat.polyx.utils

import android.os.CountDownTimer
import android.util.Log
import kotlin.properties.Delegates

abstract class PausableCountDownTimer(
    private val secondsInFuture: Int,
    private var countDownInterval: Int
) {
    private var millisRemaining: Long = (secondsInFuture * 1000 + countDownInterval).toLong()
    private var countDownTimer: CountDownTimer? = null
    private var ticksMinusOne = (1000 * secondsInFuture / countDownInterval)
    private var counter = 0
    var currentState: TimerState by Delegates.observable(
        NotStartedTimer,
        onChange = { _, oldValue, newValue ->
            Log.d(
                PausableCountDownTimer::class.simpleName,
                "Old value: $oldValue, New value: $newValue"
            )
            if ((oldValue is NotStartedTimer || oldValue is PausedTimer) && newValue is RunningTimer) {
                createCountDownTimer()
                countDownTimer?.start()
            }
            if (oldValue is RunningTimer && newValue is PausedTimer) {
                countDownTimer?.cancel()
                counter--
            }
            if ((oldValue is RunningTimer || oldValue is PausedTimer) && newValue is CanceledTimer) {
                countDownTimer?.cancel()
            }

            if (oldValue !is NotStartedTimer && newValue is NotStartedTimer) {
                countDownTimer?.cancel()
                counter = 0
                millisRemaining = (secondsInFuture * 1000 + countDownInterval).toLong()
            }
        })
        private set

    private fun createCountDownTimer() {
        countDownTimer = object : CountDownTimer(millisRemaining, countDownInterval.toLong()) {
            override fun onTick(millisUntilFinished: Long) {
                Log.d(
                    PausableCountDownTimer::class.simpleName,
                    "Millis until finished: $millisUntilFinished"
                )
                Log.d(PausableCountDownTimer::class.simpleName, "Counter: $counter")
                millisRemaining =
                    (secondsInFuture * 1000 + countDownInterval - countDownInterval * counter).toLong()
                Log.d(
                    PausableCountDownTimer::class.simpleName,
                    "New millisremaining: $millisRemaining"
                )
                Log.d(PausableCountDownTimer::class.simpleName, "Tick value: ${ticksMinusOne - counter}")
                this@PausableCountDownTimer.onTick(ticksMinusOne - counter)
                counter++
            }

            override fun onFinish() {
                Finish.let {
                    Log.d(PausableCountDownTimer::class.simpleName, "$it")
                    currentState = currentState.consumeAction(it)
                }
                this@PausableCountDownTimer.onFinish()
            }
        }
    }

    abstract fun onTick(tickCounter: Int)

    abstract fun onFinish()

    fun start(): PausableCountDownTimer {
        Start.apply {
            Log.d(PausableCountDownTimer::class.simpleName, "$this")
            currentState = currentState.consumeAction(this)
        }
        return this
    }

    fun pause() {
        Pause.apply {
            Log.d(PausableCountDownTimer::class.simpleName, "$this")
            currentState = currentState.consumeAction(this)
        }
    }

    fun cancel() {
        Cancel.apply {
            Log.d(PausableCountDownTimer::class.simpleName, "$this")
            currentState = currentState.consumeAction(this)
        }
    }

    fun reset() {
        Reset.let {
            Log.d(PausableCountDownTimer::class.simpleName, "$it")
            currentState = currentState.consumeAction(it)
        }
    }

    sealed class TimerAction

    private object Start : TimerAction() {
        override fun toString(): String {
            return "Start action"
        }
    }

    private object Pause : TimerAction() {
        override fun toString(): String {
            return "Pause action"
        }
    }

    private object Cancel : TimerAction() {
        override fun toString(): String {
            return "Cancel action"
        }
    }

    private object Finish : TimerAction() {
        override fun toString(): String {
            return "Finish action"
        }
    }

    private object Reset : TimerAction() {
        override fun toString(): String {
            return "Reset action"
        }
    }


    sealed interface TimerState {
        fun consumeAction(action: TimerAction): TimerState
    }

    object NotStartedTimer : TimerState {
        override fun consumeAction(action: TimerAction): TimerState {
            return when (action) {
                is Start -> RunningTimer
                else -> this
            }
        }

        override fun toString(): String {
            return "Not started"
        }
    }

    object RunningTimer : TimerState {
        override fun consumeAction(action: TimerAction): TimerState {
            return when (action) {
                is Start -> this
                is Pause -> PausedTimer
                is Cancel -> CanceledTimer
                is Finish -> OverTimer
                is Reset -> NotStartedTimer
            }
        }

        override fun toString(): String {
            return "Running"
        }
    }

    object PausedTimer : TimerState {
        override fun consumeAction(action: TimerAction): TimerState {
            return when (action) {
                is Start -> RunningTimer
                is Pause -> this
                is Cancel -> CanceledTimer
                is Finish -> this
                is Reset -> NotStartedTimer
            }
        }

        override fun toString(): String {
            return "Paused"
        }
    }

    object CanceledTimer : TimerState {
        override fun consumeAction(action: TimerAction): TimerState {
            return when (action) {
                is Reset -> NotStartedTimer
                else -> this
            }
        }

        override fun toString(): String {
            return "Canceled"
        }
    }

    object OverTimer : TimerState {
        override fun consumeAction(action: TimerAction): TimerState {
            return when (action) {
                is Reset -> NotStartedTimer
                else -> this
            }
        }

        override fun toString(): String {
            return "Over"
        }
    }
}

