package mx.unam.icat.polyx.model.fillintheblanksentence

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import mx.unam.icat.polyx.model.section.SectionEntity

@Entity(
    tableName = "fill_in_the_blank_sentence",
    foreignKeys = [ForeignKey(
        entity = SectionEntity::class,
        parentColumns = ["section_id"],
        childColumns = ["section_owner_id"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.SET_NULL
    )]
)
data class FillInTheBlankSentence(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "fill_in_the_blank_sentence_id")
    val fillInTheBlankSentenceId: Long,
    @ColumnInfo(name = "section_owner_id", index = true)
    val sectionOwnerId: Long?,
    val text: String,
)