package mx.unam.icat.polyx.ui.unit

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.toRoute
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import mx.unam.icat.polyx.UnitRoute
import mx.unam.icat.polyx.model.section.*
import javax.inject.Inject

@HiltViewModel
class UnitViewModel @Inject constructor(
    sectionRepository: SectionRepository,
    state: SavedStateHandle
) : ViewModel() {
    private val unitRoute = state.toRoute<UnitRoute>()
    private val _unitUiState = MutableStateFlow<UnitUiState>(UnitUiState.Loading)
    val unitUiState = _unitUiState.asStateFlow()
    private val _sheetSections =
        MutableStateFlow<List<Pair<SectionEntity, Int>>>(mutableListOf())

    init {
        viewModelScope.launch {
            val unitSectionTree = sectionRepository.getSectionSubtree(
                sectionId = unitRoute.unitId,
                includeRootSection = true)

            val titleOrTitleImageSections =
                sectionRepository.getSectionSubTreeTitleOrTitleImageTypes(
                    sectionId = unitRoute.unitId,
                    includeRootSection = true
                )
            _unitUiState.value = UnitUiState.Success(
                unitId = unitRoute.unitId,
                sections = convertToUnitSections(unitSectionTree),
                initialScrollToItemId = unitRoute.unitId
            )
            _sheetSections.value = convertToSectionSheetSections(titleOrTitleImageSections)
        }
    }

    private fun convertToUnitSections(
        sections: Map<SectionEntity, Int>,
    ): List<Pair<SectionEntity, Int>> {
        return sections.map {
            when (it.key.type) {
                SectionEntity.Type.IMAGE_TITLE -> Pair(
                    convertToTitleSection(it.key),
                    it.value
                )

                else -> Pair(it.key, it.value)
            }
        }
    }

    private fun convertToSectionSheetSections(
        sections: Map<SectionEntity, Int>
    ): List<Pair<SectionEntity, Int>> {
        return sections.map {
            when (it.key.type) {
                SectionEntity.Type.TITLE -> Pair(it.key, it.value)
                SectionEntity.Type.IMAGE_TITLE -> Pair(
                    convertToTitleSection(it.key),
                    it.value
                )

                else -> throw IllegalArgumentException()
            }
        }
    }

    private fun convertToTitleSection(section: SectionEntity): SectionEntity = section.copy(
        title2 = null,
        textContent1 = null,
        textContent2 = null,
        resourcePath1 = null,
        resourcePath2 = null,
        type = SectionEntity.Type.TITLE
    )

    fun scrollToSection(sectionId: Long?) {
        _unitUiState.update {
            if (it is UnitUiState.Success)
                it.copy(initialScrollToItemId = sectionId)
            else it
        }
    }
}

sealed interface UnitUiState {
    data object Loading : UnitUiState
    data class Success(
        val unitId: Long?,
        val sections: List<Pair<SectionEntity, Int>>,
        val initialScrollToItemId: Long?
    ) : UnitUiState
}


