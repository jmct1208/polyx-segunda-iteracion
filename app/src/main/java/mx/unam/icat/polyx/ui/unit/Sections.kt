package mx.unam.icat.polyx.ui.unit

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import mx.unam.icat.polyx.ui.AssetImage

@Composable
fun SelectableTitle(
    title: String?,
    depth: Int
) {
    if (title != null) {
        Text(text = title, style = textStyleFromDepth(depth), modifier = Modifier.fillMaxWidth())
    }
}

@Composable
fun TextContent(
    text: String?,
) {
    if (text != null) {
        Text(
            text = text,
            style = MaterialTheme.typography.body2,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
fun ImageSection(
    resource: String?,
    onClick: (String) -> Unit,
) {
    if (resource != null) {
        AssetImage(
            resourcePath = resource,
            modifier = Modifier
                .fillMaxWidth()
                .clickable(
                    onClickLabel = "Abrir imagen",
                    role = Role.Image,
                    onClick = { onClick(resource) }
                )
        )
    }
}

@Composable
fun TwoImagesSection(
    resource1: String?,
    resource2: String?,
    onClick1: (String) -> Unit,
    onClick2: (String) -> Unit,
) {
    Column(modifier = Modifier.fillMaxWidth()) {
        if (resource1 != null) {
            AssetImage(
                resourcePath = resource1,
                modifier = Modifier.align(Alignment.CenterHorizontally).clickable(
                    onClickLabel = "Abrir imagen",
                    role = Role.Image,
                    onClick = { onClick1(resource1) }
                )
            )
            Spacer(modifier = Modifier.height(16.dp))
        }
        if (resource2 != null) {
            AssetImage(
                resourcePath = resource2,
                modifier = Modifier.align(Alignment.CenterHorizontally).clickable(
                    onClickLabel = "Abrir imagen",
                    role = Role.Image,
                    onClick = { onClick2(resource2) }
                )
            )
        }
    }
}

@Preview(showBackground = true, widthDp = 360)
@Composable
fun TwoImagesSectionPreview() {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        TwoImagesSection(
            resource1 = "section_resources/propiedades_alcanos.png",
            resource2 = "section_resources/celulosa.png",
            onClick1 = {},
            onClick2 = {},
        )
    }
}

@Composable
fun TwoSubtitlesTwoTextsSection(
    title1: String?,
    title2: String?,
    text1: String?,
    text2: String?
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        if (title1 != null) {
            Text(text = title1, style = MaterialTheme.typography.subtitle1)
        }
        if (text1 != null) {
            Text(text = text1, style = MaterialTheme.typography.body2)
        }
        if (title2 != null) {
            Text(text = title2, style = MaterialTheme.typography.subtitle1)
        }
        if (text2 != null) {
            Text(text = text2, style = MaterialTheme.typography.body2)
        }
    }
}

@Composable
fun TwoSubtitlesTwoImagesSection(
    title1: String?,
    title2: String?,
    resource1: String?,
    resource2: String?,
    onClick1: (String) -> Unit,
    onClick2: (String) -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        if (title1 != null) {
            Text(text = title1, style = MaterialTheme.typography.subtitle1)
        }
        if (resource1 != null) {
            AssetImage(
                resourcePath = resource1,
                modifier = Modifier.align(Alignment.CenterHorizontally).clickable(
                    onClickLabel = "Abrir imagen",
                    role = Role.Image,
                    onClick = { onClick1(resource1) }
                )
            )
        }
        if (title2 != null) {
            Text(text = title2, style = MaterialTheme.typography.subtitle1)
        }
        if (resource2 != null) {
            AssetImage(
                resourcePath = resource2,
                modifier = Modifier.align(Alignment.CenterHorizontally).clickable(
                    onClickLabel = "Abrir imagen",
                    role = Role.Image,
                    onClick = { onClick2(resource2) }
                )
            )
        }
    }
}

@Composable
fun TwoSubtitlesTwoTextsTwoImages(
    title1: String?,
    title2: String?,
    textContent1: String?,
    textContent2: String?,
    resource1: String?,
    resource2: String?,
    onClick1: (String) -> Unit,
    onClick2: (String) -> Unit,
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        if (title1 != null) {
            Text(text = title1, style = MaterialTheme.typography.subtitle1)
        }
        if (textContent1 != null) {
            Text(text = textContent1, style = MaterialTheme.typography.body2)
        }
        if (resource1 != null) {
            AssetImage(
                resourcePath = resource1,
                modifier = Modifier.align(Alignment.CenterHorizontally).clickable(
                    onClickLabel = "Abrir imagen",
                    role = Role.Image,
                    onClick = { onClick1(resource1) }
                )
            )
        }
        if (title2 != null) {
            Text(text = title2, style = MaterialTheme.typography.subtitle1)
        }
        if (textContent2 != null) {
            Text(text = textContent2, style = MaterialTheme.typography.body2)
        }
        if (resource2 != null) {
            AssetImage(
                resourcePath = resource2,
                modifier = Modifier.align(Alignment.CenterHorizontally).clickable(
                    onClickLabel = "Abrir imagen",
                    role = Role.Image,
                    onClick = { onClick2(resource2) }
                )
            )
        }
    }
}

@Composable
fun TwoSubtitlesTwoImagesTwoTexts(
    title1: String?,
    title2: String?,
    textContent1: String?,
    textContent2: String?,
    resource1: String?,
    resource2: String?,
    onClick1: (String) -> Unit,
    onClick2: (String) -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        if (title1 != null) {
            Text(text = title1, style = MaterialTheme.typography.subtitle1)
        }
        if (resource1 != null) {
            AssetImage(
                resourcePath = resource1,
                modifier = Modifier.align(Alignment.CenterHorizontally).clickable(
                    onClickLabel = "Abrir imagen",
                    role = Role.Image,
                    onClick = { onClick1(resource1) }
                )
            )
        }
        if (textContent1 != null) {
            Text(text = textContent1, style = MaterialTheme.typography.body2)
        }
        if (title2 != null) {
            Text(text = title2, style = MaterialTheme.typography.subtitle1)
        }
        if (resource2 != null) {
            AssetImage(
                resourcePath = resource2,
                modifier = Modifier.align(Alignment.CenterHorizontally).clickable(
                    onClickLabel = "Abrir imagen",
                    role = Role.Image,
                    onClick = { onClick2(resource2) }
                )
            )
        }
        if (textContent2 != null) {
            Text(text = textContent2, style = MaterialTheme.typography.body2)
        }
    }
}

@Composable
fun textStyleFromDepth(depth: Int): TextStyle = when (depth) {
    0 -> MaterialTheme.typography.h4
    1 -> MaterialTheme.typography.h5
    2 -> MaterialTheme.typography.h6
    3 -> MaterialTheme.typography.subtitle1
    else -> MaterialTheme.typography.subtitle1
}