package mx.unam.icat.polyx.ui.course

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import mx.unam.icat.polyx.home.presentation.HorizontalThumbnailAndTextCardDivider
import mx.unam.icat.polyx.home.presentation.UnitCard
import mx.unam.icat.polyx.model.section.SectionEntity

@Composable
fun UnitList(unitList: List<SectionEntity>, navigateToUnit: (SectionEntity) -> Unit) {
    LazyColumn(Modifier.fillMaxSize()) {
        itemsIndexed(unitList) { index, unit ->
            UnitCard(unit) { navigateToUnit(it) }
            if (index < unitList.lastIndex) {
                HorizontalThumbnailAndTextCardDivider()
            }
        }
    }
}
