package mx.unam.icat.polyx.model.gameTypes

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GameTypeRepository @Inject constructor(private val gameTypeDao: GameTypeDao) {
    suspend fun getGameTypes(): List<GameType> = gameTypeDao.getGameTypes()
}