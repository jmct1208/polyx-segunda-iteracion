package mx.unam.icat.polyx.ui.quiz

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import mx.unam.icat.polyx.R
import mx.unam.icat.polyx.ui.PolyxTopBar
import mx.unam.icat.polyx.ui.theme.correctAnswerGreen
import mx.unam.icat.polyx.ui.theme.incorrectAnswerRed
import mx.unam.icat.polyx.ui.theme.noAnswerYellow

@Composable
fun CorrectAnswerdCard(
    quizAnswer: CorrectQuizAnswer,
    paddingModifier: Modifier,
    spacing: Dp,
    elevation: Dp
) {
    Surface(
        elevation = elevation,
        color = correctAnswerGreen,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = paddingModifier,
            verticalArrangement = Arrangement.spacedBy(spacing)
        ) {
            if (quizAnswer.question != null) {
                Text(
                    text = quizAnswer.question,
                    style = MaterialTheme.typography.h6,
                    modifier = Modifier.padding(start = 8.dp)
                )
            }
            Text(
                text = quizAnswer.correctOption,
                style = MaterialTheme.typography.body2,
                modifier = Modifier.padding(start = 8.dp)
            )
        }
    }
}

@Composable
fun IncorrectAnswerdCard(
    quizAnswer: IncorrectQuizAnswer,
    onStudySubunitPressed: ((Long, Long) -> Unit),
    paddingModifier: Modifier,
    spacing: Dp,
    elevation: Dp
) {
    Surface(
        elevation = elevation,
        color = incorrectAnswerRed,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = paddingModifier,
            verticalArrangement = Arrangement.spacedBy(spacing)
        ) {
            if (quizAnswer.question != null) {
                Text(
                    text = quizAnswer.question,
                    style = MaterialTheme.typography.h6,
                )
            }
            Text(
                text = quizAnswer.selectedOption,
                style = MaterialTheme.typography.body2,
            )
            Text(
                text = quizAnswer.correctOption,
                style = MaterialTheme.typography.body2,
            )
            TextButton(onClick = {
                onStudySubunitPressed(
                    quizAnswer.unitId,
                    quizAnswer.closestParentSectionId
                )
            })
            {
                Text(text = stringResource(R.string.action_study_subunit))
            }
        }
    }
}

@Composable
fun NoAnswerdCard(
    quizAnswer: NoQuizAnswer,
    onStudySubunitPressed: ((Long, Long) -> Unit),
    paddingModifier: Modifier,
    spacing: Dp,
    elevation: Dp
) {
    Surface(
        elevation = elevation,
        color = noAnswerYellow,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = paddingModifier,
            verticalArrangement = Arrangement.spacedBy(spacing)
        ) {
            if (quizAnswer.question != null) {
                Text(
                    text = quizAnswer.question,
                    style = MaterialTheme.typography.h6,
                    modifier = Modifier.padding(start = 8.dp)
                )
            }
            Text(
                text = quizAnswer.correctOption,
                style = MaterialTheme.typography.body2,
                modifier = Modifier.padding(start = 8.dp)
            )
            TextButton(onClick = {
                onStudySubunitPressed(
                    quizAnswer.unitId,
                    quizAnswer.closestParentSectionId
                )
            })
            {
                Text(text = stringResource(R.string.action_study_subunit))
            }

        }
    }
}

@Preview(showBackground = true, widthDp = 320, heightDp = 160)
@Composable
fun AnswerCardPreview() {
    /*AnswerCard(
        questionText = quizAnswer.question,
        correctAnswerText = quizAnswer.correctOption,
        incorrectAnswerText = quizAnswer.selectedOption,
        onStudySubunitPressed = {},
        sectionOwnerId = 32,
        backgroundColor = incorrectAnswerRed,
    )*/
}


@Composable
fun QuizResultsScreen(
    viewModel: QuizViewModel = hiltViewModel(),
    onStudySubunitPressed: (Long, Long) -> Unit,
    navigateUp: () -> Unit
) {
    val answers by viewModel.answerUiState.collectAsState()
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            PolyxTopBar(
                title = "Resultados",
                navigationIcon = R.drawable.ic_baseline_arrow_back_24,
                navigateUp = navigateUp
            )
        }
    ) { innerPadding: PaddingValues ->
        ResultsList(
            answers = answers,
            onStudySubunitPressed = onStudySubunitPressed,
            modifier = Modifier.padding(innerPadding)
        )
    }
}

@Composable
private fun ResultsList(
    answers: List<QuizAnswer>,
    onStudySubunitPressed: (Long, Long) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier.fillMaxSize(),
        contentPadding = PaddingValues(8.dp),
        verticalArrangement = Arrangement.spacedBy(4.dp)
    ) {
        items(answers) {
            val paddingModifier =
                Modifier.padding(start = 8.dp, end = 16.dp, top = 16.dp, bottom = 8.dp)
            val spacing = 8.dp
            when (it) {
                is CorrectQuizAnswer -> CorrectAnswerdCard(
                    quizAnswer = it,
                    paddingModifier = paddingModifier,
                    spacing = spacing,
                    elevation = 4.dp
                )

                is IncorrectQuizAnswer -> IncorrectAnswerdCard(
                    quizAnswer = it,
                    paddingModifier = paddingModifier,
                    spacing = spacing,
                    elevation = 4.dp,
                    onStudySubunitPressed = onStudySubunitPressed
                )

                is NoQuizAnswer -> NoAnswerdCard(
                    quizAnswer = it,
                    paddingModifier = paddingModifier,
                    spacing = spacing,
                    elevation = 4.dp,
                    onStudySubunitPressed = onStudySubunitPressed
                )
            }
        }
    }
}
