package mx.unam.icat.polyx.ui.gameTypeUnits

import androidx.lifecycle.*
import androidx.navigation.toRoute
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import mx.unam.icat.polyx.GameTypeUnitsRoute
import mx.unam.icat.polyx.model.gameTypes.GameTypeEnum
import mx.unam.icat.polyx.model.section.SectionEntity
import mx.unam.icat.polyx.model.section.SectionRepository
import javax.inject.Inject

@HiltViewModel
class GameTypeUnitsViewModel @Inject constructor(
    sectionRepository: SectionRepository,
    state: SavedStateHandle
) : ViewModel() {
    val route = state.toRoute<GameTypeUnitsRoute>()
    private val _units = MutableStateFlow<List<SectionEntity>>(listOf())
    val units = _units.asStateFlow()

    init {
        viewModelScope.launch {
            val units = when(GameTypeEnum.valueOf(route.gameTypeName)) {
                GameTypeEnum.QUIZ -> sectionRepository.getUnitsThatHaveQuestions(route.courseId)
                GameTypeEnum.FILL_IN_THE_BLANK -> sectionRepository.getUnitsHavingSentences(route.courseId)
                GameTypeEnum.MATCH_COLUMNS -> listOf()
            }
            _units.value = units
        }
    }
}

